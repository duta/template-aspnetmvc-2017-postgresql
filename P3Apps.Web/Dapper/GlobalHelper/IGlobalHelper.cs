﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using P3Apps.Models;


namespace P3Apps.Dapper
{
    public interface IGlobalHelper
    {
        #region Public Function
        int GetCountRowTable(string select, string NamaTable, string Where, string GroupBy);
        #endregion
        //IEnumerable<MasterPelabuhan> GetAllOrPelabuhanById(string Request, string Key, string Type);
    }
}