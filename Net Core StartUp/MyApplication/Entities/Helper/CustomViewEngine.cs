﻿using Microsoft.AspNetCore.Mvc.Razor;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using Microsoft.Extensions.Logging;
using System;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Razor.Language;
using System.Diagnostics;

namespace MyApplication.Entities
{
    public class CustomViewEngine : RazorViewEngine
    {
        public CustomViewEngine(
            IRazorPageFactoryProvider pageFactory,
            IRazorPageActivator pageActivator,
            HtmlEncoder htmlEncoder,
            IOptions<RazorViewEngineOptions> optionsAccessor,
            RazorProject razorProject,
            ILoggerFactory loggerFactory,
            DiagnosticSource diagnosticSource)
            : base(pageFactory,
                  pageActivator,
                  htmlEncoder,
                  optionsAccessor,
                  razorProject,
                  loggerFactory,
                  diagnosticSource)
        { }

        public IList<string> ViewLocationFormats
        {
            get
            {
                var origLocations = ViewLocationFormats;

                //MOCK: Replace with real-world condition
                var rand = new Random();
                var randomCondition = (rand.Next(0, 2) == 0);

                var MyExtension = randomCondition ? ".html" : ".aspx";

                /*
                * ViewLocationFormats contains the list of view location.
                * Replace the name of views with the vary extension
                ^ it Concats the orginal locations to maintain previous paths
                */
                return ViewLocationFormats.Select(f => f.Replace(".cshtml", MyExtension + ".cshtml")).Concat(origLocations).ToList();
            }
        }

    }
}
