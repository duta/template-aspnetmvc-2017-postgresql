﻿using System.Collections.Generic;

namespace MyApplication.Filters
{
    public class ValidateOriginOptions
    {
        public List<string> WhitelistedOrigins { get; set; }
    }
}
