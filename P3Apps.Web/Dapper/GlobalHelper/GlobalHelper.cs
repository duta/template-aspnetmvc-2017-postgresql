﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using P3Apps.Models;
using Dapper;

namespace P3Apps.Dapper
{
    public class GlobalHelper : IGlobalHelper
    {
        //private IDbConnection _dbTOSGC = new NpgsqlConnection(DatabaseHelper.GetConnectionString("TOSGC"));
        private IDbConnection _dbTPSOL = new NpgsqlConnection(DatabaseHelper.GetConnectionString());

        // Get:CountRowTable
        #region Public Function
        public int GetCountRowTable(string select, string NamaTable, string Where , string GroupBy)
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(DatabaseHelper.GetConnectionString()))
            {
                connection.Open();
                NpgsqlCommand command = connection.CreateCommand();
                Int32 Response = 0;
                try
                {
                    command.CommandText = "SELECT "+ select +"  FROM " + NamaTable + Where + GroupBy;
                    Response = Convert.ToInt32(command.ExecuteScalar());
                    return (int)Response;
                }
                catch (Exception)
                {
                    return (int)Response;
                }
            }
        }
        #endregion

        // GET: PublicHalper
        //public IEnumerable<MasterPelabuhan> GetAllOrPelabuhanById(string Request, string Key, string Type)
        //{
        //    string query = "";
        //    IEnumerable<MasterPelabuhan> Response = null;
        //    try
        //    {
        //        switch (Type)
        //        {
        //            case "Like":
        //                query = "SELECT DISTINCT MPLB_KODE,MPLB_NAMA FROM V_MASTER_PELABUHAN WHERE UPPER(MPLB_NAMA) LIKE '%' || :MPLB_NAMA || '%' AND KD_CABANG = :KD_CABANG ORDER BY MPLB_NAMA ASC";
        //                Response = this._dbTOSGC.Query<MasterPelabuhan>(query, new { MPLB_NAMA = Request, KD_CABANG = Key }).ToList();
        //                break;
        //            case "ById":
        //                query = "SELECT DISTINCT MPLB_KODE,MPLB_NAMA FROM V_MASTER_PELABUHAN WHERE MPLB_KODE =:MPLB_KODE AND KD_CABANG = :KD_CABANG ORDER BY MPLB_NAMA ASC";
        //                Response = this._dbTOSGC.Query<MasterPelabuhan>(query, new { MPLB_KODE = Request, KD_CABANG = Key }).ToList();
        //                break;
        //            case "All":
        //                query = "SELECT DISTINCT MPLB_KODE,MPLB_NAMA FROM V_MASTER_PELABUHAN WHERE KD_CABANG = :KD_CABANG ORDER BY MPLB_NAMA ASC";
        //                Response = this._dbTOSGC.Query<MasterPelabuhan>(query, new { KD_CABANG = Key }).ToList();
        //                break;
        //        }
        //        return Response;
        //    }
        //    catch (Exception)
        //    {
        //        return Response;
        //    }
        //}

    }
}