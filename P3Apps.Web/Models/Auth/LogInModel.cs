﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace P3Apps.Models
{
    public class LogInModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string ReturnUrl { get; set; }
    }
}