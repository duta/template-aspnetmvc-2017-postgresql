﻿var FormValidationMd = function () {
    var handleValidation1 = function () {
        // for more info visit the official plugin documentation: 
        var form1 = $('#form_validation_1');
        var error1 = $('.alert-danger', form1);

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null
        };

        $(".button-clear").click(function () {
            $('#form_validation_1 :input').val('');
            $('.form-group').removeClass('has-success');
            $('.form-group').removeClass('has-error');
            $('.help-block-error').remove();
            error1.hide();
        });

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            messages: {
                Menu_Nama: {
                    maxlength: jQuery.validator.format("Maksimal 100 Karakter"),
                    required: "Data Harus Diisi"
                },
                Menu_Alias: {
                    maxlength: jQuery.validator.format("Maksimal 250 Karakter"),
                    required: "Data Harus Diisi"
                },
                Menu_Url: {
                    maxlength: jQuery.validator.format("Maksimal 250 Karakter")
                },
                //Menu_Parent_ID: {
                //    maxlength: jQuery.validator.format("Maksimal 100 Karakter")
                //},
                Menu_Icon: {
                    maxlength: jQuery.validator.format("Maksimal 20 Karakter")
                }
            },
            rules: {
                Menu_Nama: {
                    maxlength: 100,
                    required: true
                },
                Menu_Alias: {
                    maxlength: 250,
                    required: true
                },
                Menu_Url: {
                    maxlength: 250
                },
                //Menu_Parent_ID: {
                //    maxlength:
                //},
                Menu_Icon: {
                    maxlength: 20
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                error1.show();
                App.scrollTo(error1, -200);
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element); // for other inputs, just perform default behavior
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
                label
                    .closest('.form-group').addClass('has-success');
            },

            submitHandler: function (form) {
                error1.hide();
                form.submit();
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleValidation1();
        }
    };

}();

var UINestable = function () {

    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');

        var dataJson1 = window.JSON.stringify(list.nestable('serialize'));
      //  console.log(dataJson1);

        var dataJson = [];
        var urutan = 1;

        var Recuresive = function (v, vid) {
            $.each(v.children, function (c, d) {
                var did_countstr = d.id.indexOf(":") + 1;
                var did = d.id.substr(did_countstr);
                dataJson.push({ "id": parseInt(did), "parentid": vid, "urutan": parseInt(urutan + 1) });
                urutan++;
                Recuresive(d, did);
            });
        };

        $.each(list.nestable('serialize'), function (e, v) {
            var vid_countstr = v.id.indexOf(":") + 1;
            var vid = v.id.substr(vid_countstr);
            dataJson.push({ "id": parseInt(vid), "urutan": parseInt(urutan) });
          
            Recuresive(v,vid);
            urutan++;
        });
      
        //console.log(JSON.stringify(dataJson));

        return JSON.stringify(dataJson);
    };
    return {
        //main function to initiate the module
        init: function () {
            $("#save-menu").click(function () {

                App.blockUI({
                    boxed: true,
                    message: 'Menyimpan data...'
                });

                $.ajax({
                    type: "POST",
                    url: "/Settings/Menu_StoreOrderMenu",
                    contentType: "application/json; charset=utf-8",
                    data: updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output'))),
                    headers: {
                        'CSRF-TOKEN_PELINDO3': $("[name=csrf-token]").attr("content")
                    },
                    success: function (data) {
                        if (data.Response_Type == "S") {
                            App.unblockUI();
                            toastr.success(data.Response_Text, "Sukses");
                            setInterval(function () {
                                location.href = "/Settings/Menu";
                            }, 1000);
                        } else {
                            App.unblockUI();
                            toastr.error(data.Response_Text, "Error Kode : " + data.Response_Type);
                        }
                    },
                    error: function () {
                        App.unblockUI();
                        toastr.error("Gagal mengubah strukture menu", "Error");
                    },
                });
            });

            // activate Nestable for list 1
            $('#nestable_list_1').nestable({
                group: 1
            })
                .on('change', updateOutput);

            // output initial serialised data
            updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));

            $('#nestable_list_menu').on('click', function (e) {
                var target = $(e.target),
                    action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });
        }
    };
}();

function ConfirmDelete(action) {
    $("#form_delete").attr('action', action);
    $('#DeleteModals').modal('show');
}

jQuery(document).ready(function () {
    UINestable.init();
    FormValidationMd.init();
});