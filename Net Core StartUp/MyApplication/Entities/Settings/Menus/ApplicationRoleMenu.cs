﻿using Microsoft.AspNetCore.Identity;
using MyApplication.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyApplication.Entities
{
    public class ApplicationRoleMenu 
    {
        public ApplicationRoleMenu()
        {
            Permissions = new HashSet<MenuPermission>();
        }

        [Key, Column(Order = 1), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual string Id { get; set; }

        //[Column(Order = 2)]
        //public virtual string RoleId { get; set; }

        //[Column(Order = 3)]
        //public virtual string MenuId { get; set; }
        //[Column(Order = 4)]
        //public virtual string PermissionId { get; set; }

        public virtual string Kd_cabang { get; set; }
        public virtual string Kd_Terminal { get; set; }
        public virtual ApplicationRole Role { get; set; }
        public virtual Permission Permission { get; set; }
        public virtual MenuItem Menu { get; set; }
        public ICollection<MenuPermission> Permissions { get; set; }
    }
}
