﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using P3Apps.Dapper;
using P3Apps.Models;
using log4net;
using System.Web.Routing;

namespace P3Apps.Controllers
{
    public class HomeController : Controller
    {
        private IHome _Dashboard = new Home();
        private IGlobalHelper _GlobalHelper = new GlobalHelper();
        private static readonly ILog _Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ActionResult Index()
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    string
                          username = claimsIdentity.FindFirst("USERNAME").Value,
                          hakakses = claimsIdentity.FindFirst("HAKAKSES").Value;
                    ViewBag.USERNAME = username;
                    ViewBag.HAKAKSES = hakakses;

                    _Logger.Info("By " + username + " : Dashboard -> Success returning to Home/Index");
                    return View();
                }
                else
                {
                    _Logger.Error("Dashboard -> Mohon maaf permintaan kami tolak karena anda belum login");
                   return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
                }
            }
            catch (Exception e)
            {
                _Logger.Fatal("Dashboard -> " + e.Message.ToString());
               return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
            }
        }
        
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}