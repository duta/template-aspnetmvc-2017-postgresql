﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MyApplication.Models;

namespace MyApplication.Entities
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public DbSet<ApplicationRoleMenu> RoleMenus { get; set; }
        public DbSet<MenuPermission> MenuPermis { get; set; }
        public DbSet<MenuItem> Menus { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<IdentityUserClaim<string>> UserClaim { get; set; }
        public DbSet<IdentityUserLogin<string>> UserLogin { get; set; }
        public DbSet<IdentityUserRole<string>> UserRole { get; set; }
        public DbSet<IdentityUserToken<string>> UserToken { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>().ToTable("App_Users");
            builder.Entity<ApplicationRole>().ToTable("App_Roles");
            builder.Entity<MenuPermission>().ToTable("App_MenuPermission");
            builder.Entity<MenuItem>().ToTable("App_Menus");
            builder.Entity<Permission>().ToTable("App_Permissions");
            builder.Entity<ApplicationRoleMenu>().ToTable("App_RoleMenus");
            builder.Entity<IdentityUserRole<string>>().ToTable("App_UserRoles");
            builder.Entity<IdentityUserClaim<string>>().ToTable("App_UserClaims");
            builder.Entity<IdentityRoleClaim<string>>().ToTable("App_RoleClaims");
            builder.Entity<IdentityUserLogin<string>>().ToTable("App_UserLogins");
            builder.Entity<IdentityUserToken<string>>().ToTable("App_UserTokens");
        }
    }
}
