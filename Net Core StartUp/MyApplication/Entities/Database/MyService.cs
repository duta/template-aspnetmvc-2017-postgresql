﻿using Microsoft.Extensions.Configuration;

namespace MyApplication.Entities
{
    public class MyService
    {
        // For Manage All Service Connection String 
        public static IConfiguration Configuration;

        public MyService(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public static string GetConnectionString(string name = "DefaultConnection")
        {
            return Configuration.GetConnectionString(name);
        }
        public static string GetEndPoint(string name = "DefaultService")
        {
            return Configuration.GetConnectionString(name);
        }
    }
}
