﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace P3Apps.Models
{
    public class MenuModels 
    {
        public int Menu_ID { get; set; }
        public string Menu_Nama { get; set; }
        public string Menu_Url { get; set; }
        public string Menu_Alias { get; set; }
        public int? Menu_Parent_ID { get; set; }
        public string Menu_Icon { get; set; }
        public int Menu_URUT { get; set; }
    }
}