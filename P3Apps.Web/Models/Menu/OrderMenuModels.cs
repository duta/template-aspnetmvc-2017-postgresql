﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace P3Apps.Models
{
    public class OrderMenuModels
    {
        public int id { get; set; }
        public string parentid { get; set; }
        public int urutan { get; set; }
    }
}