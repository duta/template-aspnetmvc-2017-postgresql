﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyApplication.Entities;
using MyApplication.Models;
using Microsoft.AspNetCore.Authorization;

namespace MyApplication.Controllers
{
    [Authorize]
    [Area("Settings")]
    public class PermissionController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PermissionController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Permission
        public async Task<IActionResult> Index()
        {
            return View(await _context.Permissions.ToListAsync());
        }

        // GET: Permission/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var permission = await _context.Permissions
                .SingleOrDefaultAsync(m => m.Id == id);
            if (permission == null)
            {
                return NotFound();
            }

            return View(permission);
        }

        // GET: Permission/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Permission/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Permission permission)
        {
            //if (ModelState.IsValid)
            //{
                Permission Permission = new Permission { };
                if (Permission != null)
                {
                    Permission.Name = permission.Name;
                    Permission.CreatedBy = User.Identity.Name;
                    Permission.CreatedDate = DateTime.Now;
                    await _context.Permissions.AddAsync(Permission);
                }

                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            //}
            //return View(permission);
        }

        // GET: Permission/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var permission = await _context.Permissions.SingleOrDefaultAsync(m => m.Id == id);
            if (permission == null)
            {
                return NotFound();
            }
            return View(permission);
        }

        // POST: Permission/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id,Permission permission)
        {
            if (id != permission.Id)
            {
                return NotFound();
            }

            //if (ModelState.IsValid)
            //{
                try
                {
                    Permission Permission = _context.Permissions.Find(id);
                    Permission.Name = permission.Name;
                    Permission.UpdatedBy = User.Identity.Name;
                    Permission.UpdatedDate = DateTime.Now;
                    _context.Update(Permission);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PermissionExists(permission.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            //}
            //return View(permission);
        }

        // GET: Permission/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var permission = await _context.Permissions
                .SingleOrDefaultAsync(m => m.Id == id);
            if (permission == null)
            {
                return NotFound();
            }

            return View(permission);
        }

        // POST: Permission/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var permission = await _context.Permissions.SingleOrDefaultAsync(m => m.Id == id);
            _context.Permissions.Remove(permission);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PermissionExists(string id)
        {
            return _context.Permissions.Any(e => e.Id == id);
        }
    }
}
