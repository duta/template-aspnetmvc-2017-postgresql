﻿(function ($) {
    function Application() {
        var $this = this;

        function initilizeModel() {
            $("#modal-action").on('show.bs.modal', function (e) {
                $(this).find('.modal-content').load(e.relatedTarget.href);
            }).on('hidden.bs.modal', function (e) {
                $(this).removeData('bs.modal');
            });
        }
        $this.init = function () {
            initilizeModel();
        }
    }
    $(function () {
        var self = new Application();
        self.init();
    })
}(jQuery))
