﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.AspNetCore.Authorization;
//using DataTables.AspNet.Core;
//using DataTables.AspNet.AspNetCore;
//using SpinerService;
//using System.Security.Claims;

//namespace MyApplication.Controllers.VesselPlanning
//{

//    [Authorize]
//    [Area("VesselPlanning")]
//    public class VoyageNotificationController : Controller
//    {
//        private PelindoServiceClient _Service = new PelindoServiceClient();

//        // GET: VesselScheduleEstimate
//        public ActionResult Index()
//        {
//            return View();
//        }

//        // GET: VesselScheduleEstimate/Details/5
//        public ActionResult Details(int id)
//        {
//            return View();
//        }

//        // GET: VesselScheduleEstimate/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: VesselScheduleEstimate/Create
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public async Task<StringResponse> Create([FromBody]SaveVesScheduleEstRequest collection)
//        {
//            StringResponse Response = new StringResponse();

//            if (ModelState.IsValid)
//            {
//                var _CI = User.Identity as ClaimsIdentity;
//                Response = await _Service.SaveVesScheduleEstAsync(new SaveVesScheduleEstRequest
//                {
//                    XKdCabang = int.Parse(_CI.FindFirst("KD_CABANG").Value),
//                    XKdTerminal = int.Parse(_CI.FindFirst("KD_TERMINAL").Value),
//                    XVesCode = collection.XVesCode,
//                    XVesName = collection.XVesName,
//                    XAgent = collection.XAgent,
//                    XPBM = collection.XPBM,
//                    XLOA = collection.XLOA,
//                    XKeterangan = collection.XKeterangan,
//                    XStatus = 1,
//                    XEstArrivalTime = collection.XEstArrivalTime,
//                    XEstDepartureTime = collection.XEstDepartureTime,
//                    ZKdFrom = collection.ZKdFrom,
//                    ZKdTo = collection.ZKdTo,
//                    XLastPort = collection.XLastPort,
//                    XNextPort = collection.XNextPort,
//                    XOriginPort = User.Identity.Name,
//                    XDestPort = collection.XDestPort,
//                    XCreatedPort = collection.XCreatedPort,
//                    XBerthNo = collection.XBerthNo,
//                    XGt = collection.XGt,
//                    XBendera = collection.XBendera,
//                    XPalka = collection.XPalka,
//                    XPalkaType = collection.XPalkaType,
//                    XLastPortName = collection.XLastPortName,
//                    XNextPortName = collection.XNextPortName,
//                    XOriginPortName = collection.XOriginPortName,
//                    XDestPortName = collection.XDestPortName,
//                    XBerthName = collection.XBerthName,
//                    XProgramName = _CI.FindFirst("APP_NAME").Value,
//                    Validated = new IsValidated
//                    {
//                        GUID = _CI.FindFirst("GUID").Value,
//                        USERNAME = _CI.FindFirst("USERNAME").Value,
//                        TOKEN = _CI.FindFirst("TOKEN").Value
//                    }
//                });

//                return Response;
//            }
//            else
//            {
//                return new StringResponse { Status = "EV", Result = null, Message = "Your Input data is not Valid. #02" };
//            }
//        }

//        // GET: VesselScheduleEstimate/Edit/5
//        public ActionResult Edit(int id)
//        {
//            return View();
//        }

//        // POST: VesselScheduleEstimate/Edit/5
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit(int id, IFormCollection collection)
//        {
//            try
//            {
//                // TODO: Add update logic here

//                return RedirectToAction(nameof(Index));
//            }
//            catch
//            {
//                return View();
//            }
//        }

//        // GET: VesselScheduleEstimate/Delete/5
//        public ActionResult Delete(int id)
//        {
//            return View();
//        }

//        // POST: VesselScheduleEstimate/Delete/5
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Delete(int id, IFormCollection collection)
//        {
//            try
//            {
//                // TODO: Add delete logic here

//                return RedirectToAction(nameof(Index));
//            }
//            catch
//            {
//                return View();
//            }
//        }

//        //public IActionResult PageData(IDataTablesRequest request)
//        //{
//        //    //// Nothing important here. Just creates some mock data.
//        //    //var data = Models.SampleEntity.GetSampleData();

//        //    //// Global filtering.
//        //    //// Filter is being manually applied due to in-memmory (IEnumerable) data.
//        //    //// If you want something rather easier, check IEnumerableExtensions Sample.
//        //    //var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
//        //    //    ? data
//        //    //    : data.Where(_item => _item.Name.Contains(request.Search.Value));

//        //    //// Paging filtered data.
//        //    //// Paging is rather manual due to in-memmory (IEnumerable) data.
//        //    //var dataPage = filteredData.Skip(request.Start).Take(request.Length);

//        //    //// Response creation. To create your response you need to reference your request, to avoid
//        //    //// request/response tampering and to ensure response will be correctly created.
//        //    //var response = DataTablesResponse.Create(request, data.Count(), filteredData.Count(), dataPage);

//        //    //// Easier way is to return a new 'DataTablesJsonResult', which will automatically convert your
//        //    //// response to a json-compatible content, so DataTables can read it when received.
//        //    //return new DataTablesJsonResult(response, true);
//        //}
//    }
//}