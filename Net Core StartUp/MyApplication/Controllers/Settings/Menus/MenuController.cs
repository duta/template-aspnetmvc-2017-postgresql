﻿using MyApplication.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using MyApplication.Entities;
using Microsoft.EntityFrameworkCore;

namespace MyApplication.Controllers
{
    [Authorize]
    [Area("Settings")]
    public class MenuController : Controller
    {
        private ApplicationDbContext _Context;

        public MenuController(ApplicationDbContext context)
        {
            this._Context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            List<MenuItem> model = new List<MenuItem>();
            model = _Context.Menus.ToList();
            return View(model);
        }
        [HttpGet]
        public IActionResult AddEditMenu(string id)
        {
            MenuItem model = new MenuItem();
            if (!String.IsNullOrEmpty(id))
            {
                MenuItem Menu = _Context.Menus.SingleOrDefault(c => c.Id == id);
                if (Menu != null)
                {
                    model = Menu;
                }
            }
            return PartialView("_AddEditMenu", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddEditMenu(string id, MenuItem model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool isExist = !String.IsNullOrEmpty(id);
                    MenuItem Menu = isExist ? _Context.Menus.Find(id) : new MenuItem { };
                    if (Menu != null)
                    {
                        Menu.Name = model.Name;
                        Menu.Url = model.Url;
                        Menu.ParentId = model.ParentId;
                        Menu.Icon = model.Icon;
                        Menu.Order = model.Order;
                        Menu.Category = model.Category;

                        if (isExist)
                        {
                            Menu.UpdatedBy = User.Identity.Name;
                            Menu.UpdatedDate = DateTime.Now;
                            _Context.Menus.Update(Menu);
                        }
                        else
                        {
                            Menu.CreatedBy = User.Identity.Name;
                            Menu.CreatedDate = DateTime.Now;
                            await _Context.Menus.AddAsync(Menu);
                        }
                    }
                    
                    await _Context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> DeleteMenu(string id)
        {
            MenuItem menu = new MenuItem();
            if (!String.IsNullOrEmpty(id))
            {
                MenuItem Menu = await _Context.Menus.FindAsync(id);
                if (Menu != null)
                {
                    menu.Id = id;
                    menu.Name = Menu.Name;
                }
            }
            return PartialView("_DeleteMenu", menu);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteMenu(string id, IFormCollection form)
        {
            try
            {
                if (!String.IsNullOrEmpty(id))
                {
                    MenuItem Menu = await _Context.Menus.FindAsync(id);
                    if (Menu != null)
                    {
                        _Context.Menus.Remove(Menu);
                        await _Context.SaveChangesAsync();
                    }
                }
            } catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
