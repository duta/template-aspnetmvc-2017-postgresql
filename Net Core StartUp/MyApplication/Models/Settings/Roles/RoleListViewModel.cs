﻿namespace MyApplication.Models
{
    public class RoleListViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int NumberOfUsers { get; set; }
    }
}
