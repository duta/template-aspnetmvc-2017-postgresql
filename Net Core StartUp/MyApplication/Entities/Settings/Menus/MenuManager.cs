﻿using Microsoft.EntityFrameworkCore;
using MyApplication.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MyApplication.Entities
{

    public class MenuManager
    {
        private readonly ApplicationDbContext _context;

        public MenuManager(ApplicationDbContext context)
        {
            _context = context;
        }

        public ICollection<MenuPermission> GetMenuPermissionsByUser(string user)
        {
            if (user == null)
            {
                return new Collection<MenuPermission>();
            }

            return _context.Roles
                //.Include(role => role.MenuItems.Select(menu => menu.Permissions))
                .Where(role => role.User.UserId == user)
                .SelectMany(role => role.MenuItems.SelectMany(roleMenu => roleMenu.Permissions))
                .ToList();
        }


        public ICollection<MenuItem> GetMenuByUser(string user,
            Func<MenuPermission, bool> filterFunc = null)
        {
            var items = GetMenuPermissionsByUser(user);

            var records = filterFunc == null
                ? items.ToList()
                : items.Where(filterFunc).ToList();

            return records
                .GroupBy(menuPermission => menuPermission.RoleMenu.Menu)
                .Select(grouping => grouping.Key)
                .ToList();
        }

        public ICollection<MenuItem> GetAllByUser(string user)
        {
            return GetMenuByUser(user);
        }

        public IEnumerable<MenuItem> GetViewableMenuItems(string user)
        {
            return GetMenuByUser(user, menuPermission => menuPermission.Permission.Name == "View");
        }

        public ICollection<MenuItem> GetCreateMenuItems(string user)
        {
            return GetMenuByUser(user, menuPermission => menuPermission.Permission.Name == "Create");
        }

        public ICollection<MenuItem> GetDeleteMenuItems(string user)
        {
            return GetMenuByUser(user, menuPermission => menuPermission.Permission.Name == "Delete");
        }

        public ICollection<MenuItem> GetUpdateMenuItems(string user)
        {
            return GetMenuByUser(user, menuPermission => menuPermission.Permission.Name == "Update");
        }

        public ICollection<MenuItem> GetUploadMenuItems(string user)
        {
            return GetMenuByUser(user, menuPermission => menuPermission.Permission.Name == "Upload");
        }

        public ICollection<MenuItem> GetPublishMenuItems(string user)
        {
            return GetMenuByUser(user, menuPermission => menuPermission.Permission.Name == "Publish");
        }

    }
}
