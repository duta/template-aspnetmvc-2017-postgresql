﻿using MyApplication.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyApplication.Entities
{
    public class MenuPermission
    {

        [Key,Column(Order = 1)]
        public virtual string RoleMenuId { get; set; }

        [Column(Order = 2)]
        public virtual string PermissionId { get; set; }

        public virtual Permission Permission { get; set; }
        public virtual ApplicationRoleMenu RoleMenu { get; set; }
    }
}
