﻿using Microsoft.AspNetCore.Identity;

namespace MyApplication.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
    }
}
