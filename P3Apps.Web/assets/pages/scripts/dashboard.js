var Dashboard = function () {
    var Start = "", End = "", Label = "";

    //var PublicFunction = function () {
    //    $('[name=StagingTerkirim]').on("click", function () {
    //        $('#ShowTransaksi').modal('show');
    //        $('#ShowTransaksi h4.modal-title').html("<div class='text-success'><i class='icon-like' style='font-size:24px;'></i> Transaksi Terkirim </div>");
    //        $('#ShowTransaksi div.alert-info').html("<i class='fa fa-info-circle' style='font-size:24px;'></i> Filter <b>" + Label + "</b>, Mulai dari tanggal <b>" + moment(Start, 'DD/MM/YYYY HH:mm:ss').format('DD MMMM YYYY HH:mm:ss A') + "</b> sampai <b>" + moment(End, 'DD/MM/YYYY HH:mm:ss').format('DD MMMM YYYY HH:mm:ss A')) + "</b>";
    //        HandleTable(1, Start, End);
    //    });
    //    $('[name=StagingPending]').on("click", function () {
    //        $('#ShowTransaksi').modal('show');
    //        $('#ShowTransaksi h4.modal-title').html("<div class='text-danger'><i class='icon-cloud-upload' style='font-size:24px;'></i> Transaksi Pending </div>");
    //        $('#ShowTransaksi div.alert-info').html("<i class='fa fa-info-circle' style='font-size:24px;'></i> Filter <b>" + Label + "</b>, Mulai dari tanggal <b>" + moment(Start, 'DD/MM/YYYY HH:mm:ss').format('DD MMMM YYYY HH:mm:ss A') + "</b> sampai <b>" + moment(End, 'DD/MM/YYYY HH:mm:ss').format('DD MMMM YYYY HH:mm:ss A')) + "</b>";
    //        HandleTable(0, Start, End);
    //    });
    //};

    //var HandleTable = function (Status, Start, End) {
    //    $('#datatable').DataTable({
    //        "destroy": true,
    //        "processing": true,
    //        //"serverSide": true,
    //        "language": { "url": "/assets/global/plugins/datatables/plugins/i18n/Indonesian.json" },
    //        "ajax": {
    //            "url": window.location.origin + "/Home/GetTransaksi",
    //            "type": "POST",
    //            "datatype": "json",
    //            "data": { Status: Status, Start: Start, End: End },
    //            "headers": {
    //                'CSRF-TOKEN_PELINDO3': $("[name=csrf-token]").attr("content")
    //            }
    //        },
    //        "columns": [
    //            { "data": "NO_PPKB1", "name": "NO_PPKB1" },
    //            { "data": "NO_PERMOHONAN", "name": "NO_PERMOHONAN" },
    //            { "data": "REF_NUMBER", "name": "REF_NUMBER" },
    //            { "data": "NM_KAPAL", "name": "NM_KAPAL" },
    //            { "data": "NM_PBM", "name": "NM_PBM" },
    //            {
    //                "data": "CREATION_DATE", "name": "CREATION_DATE",
    //                "render": function (data, type, full, meta) {
    //                    return moment(data, '').format("DD MMMM YYYY");
    //                }
    //            },
    //        ],
    //        "order": [
    //            [5,"desc"]
    //        ],
    //        "lengthMenu": [
    //            [5, 10, 25, 50, 100, -1],
    //            [5, 10, 25, 50, 100, "Semua"] // change per page values here
    //        ],
    //        // set the initial value
    //        "pageLength": 5,
    //        "pagingType": 'bootstrap_extended'
    //    });
    //}

    //var handleCounterup = function () {
    //    if (!$().counterUp) {
    //        return;
    //    }

    //    $("[data-counter='counterup']").counterUp({
    //        delay: 10,
    //        time: 1000
    //    });
    //};

    //var GetChartStagingMonth = function () {
    //    App.blockUI({
    //        boxed: true,
    //        message: 'Loading data...'
    //    });
    //    var Return;
    //    $.ajax({
    //        type: "POST",
    //        url: window.location.origin + "/Home/GetChartStagingMonth",
    //        async: false,
    //        contentType: 'application/json; charset=utf-8',
    //        headers: {
    //            'CSRF-TOKEN_PELINDO3': $("[name=csrf-token]").attr("content")
    //        },
    //        success: function (data) {
    //            if (data.Response_Type == "S") {
    //                Return = data.Response;
    //                App.unblockUI();
    //            } else if (data.Response_Type == "EQ") {
    //                toastr.warning(data.Response_Text, 'Terjadi Kesalahan');
    //                App.unblockUI();
    //            } else if (data.Response_Type == "EL") {
    //                toastr.warning(data.Response_Text, 'Terjadi Kesalahan');
    //                App.unblockUI();
    //            } else {
    //                toastr.error(data.Response_Text, 'Terjadi Kesalahan');
    //                App.unblockUI();
    //            }
    //        },
    //        error: function (data) {
    //            toastr.error('Error Send Ajax Karena : ' + data.Response_Text, 'Error Kode : ' + data.Response_Type);
    //            App.unblockUI();
    //        }
    //    });
    //    return Return;
    //}

    //var GetChartStagingYear = function () {
    //    App.blockUI({
    //        boxed: true,
    //        message: 'Loading data...'
    //    });
    //    var Return;
    //    $.ajax({
    //        type: "POST",
    //        url: window.location.origin + "/Home/GetChartStagingYear",
    //        async: false,
    //        contentType: 'application/json; charset=utf-8',
    //        headers: {
    //            'CSRF-TOKEN_PELINDO3': $("[name=csrf-token]").attr("content")
    //        },
    //        success: function (data) {
    //            if (data.Response_Type == "S") {
    //                Return = data.Response;
    //                App.unblockUI();
    //            } else if (data.Response_Type == "EQ") {
    //                toastr.warning(data.Response_Text, 'Terjadi Kesalahan');
    //                App.unblockUI();
    //            } else if (data.Response_Type == "EL") {
    //                toastr.warning(data.Response_Text, 'Terjadi Kesalahan');
    //                App.unblockUI();
    //            } else {
    //                toastr.error(data.Response_Text, 'Terjadi Kesalahan');
    //                App.unblockUI();
    //            }
    //        },
    //        error: function (data) {
    //            toastr.error('Error Send Ajax Karena : ' + data.Response_Text, 'Error Kode : ' + data.Response_Type);
    //            App.unblockUI();
    //        }
    //    });
    //    return Return;
    //}

    //var GetStaging = function (Status, Start, End) {
    //    App.blockUI({
    //        boxed: true,
    //        message: 'Loading data...'
    //    });
    //    var Return = 0;
    //    $.ajax({
    //        type: "POST",
    //        url: window.location.origin + "/Home/GetDashboard",
    //        async: false,
    //        data: JSON.stringify({ Status: Status, Start: Start, End: End }),
    //        contentType: 'application/json; charset=utf-8',
    //        headers: {
    //            'CSRF-TOKEN_PELINDO3': $("[name=csrf-token]").attr("content")
    //        },
    //        success: function (data) {
    //            if (data.Response_Type == "S") {
    //                Return = data.Response_Data;
    //                App.unblockUI();
    //            } else if (data.Response_Type == "EQ") {
    //                toastr.warning(data.Response_Text, 'Terjadi Kesalahan');
    //                App.unblockUI();
    //            } else if (data.Response_Type == "EL") {
    //                toastr.warning(data.Response_Text, 'Terjadi Kesalahan');
    //                App.unblockUI();
    //            } else {
    //                toastr.error(data.Response_Text, 'Terjadi Kesalahan');
    //                App.unblockUI();
    //            }
    //        },
    //        error: function (data) {
    //            toastr.error('Error Send Ajax Karena : ' + data.Response_Text, 'Error Kode : ' + data.Response_Type);
    //            App.unblockUI();
    //        }
    //    });
    //    return Return;
    //}

    //var GetDashboard = function (Start, End) {
    //    var Val0 = GetStaging('0', Start, End);
    //    var Val1 = GetStaging('1', Start, End);

    //    $("#Staging0").attr("data-value", Val0);
    //    $("#Staging1").attr("data-value", Val1);
        
    //    Val0 == 0 ? $("[name=StagingPending]").addClass("hide") : $("[name=StagingPending]").removeClass("hide");
    //    Val1 == 0 ? $("[name=StagingTerkirim]").addClass("hide") : $("[name=StagingTerkirim]").removeClass("hide");

    //    handleCounterup();
    //};

    return {

        initDashboardDaterange: function() {
            if (!jQuery().daterangepicker) {
                return;
            }

            $('#dashboard-report-range').daterangepicker({
                "ranges": {
                    '7 Hari Terakhir': [moment().subtract('days', 6), moment()],
                    'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                    '3 Bulan Lalu': [moment().subtract('month', 2).startOf('month'), moment().endOf('month')],
                    '6 Bulan Lalu': [moment().subtract('month', 5).startOf('month'), moment().endOf('month')],
                    'Tahun Ini': [moment().startOf('year'), moment().endOf('year')]
                },
                "locale": {
                    "format": "DD/MM/YYYY HH:mm:ss",
                    "separator": " - ",
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Dari",
                    "toLabel": "Ke",
                    "customRangeLabel": "Pilih Sendiri",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Augustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
                "startDate": moment().subtract('days', 6),
                "endDate": moment(),
                opens: (App.isRTL() ? 'right' : 'left'),
            }, function(start, end, label) {
                if ($('#dashboard-report-range').attr('data-display-range') != '0') {
                    $('#dashboard-report-range span').html(start.format('DD MMMM YYYY') + ' - ' + end.format('DD MMMM YYYY'));
                }

                $('div.status-title').text(label);

                //GetDashboard(start.format('DD/MM/YYYY HH:mm:ss'), end.format('DD/MM/YYYY HH:mm:ss'));
                Start = start.format('DD/MM/YYYY HH:mm:ss');
                End = end.format('DD/MM/YYYY HH:mm:ss');
                Label = label;
            });
             if ($('#dashboard-report-range').attr('data-display-range') != '0') {
                 $('#dashboard-report-range span').html(moment().subtract('days', 29).format('DD MMMM YYYY') + ' - ' + moment().format('DD MMMM YYYY'));

                 Start = moment().startOf('day').subtract('days', 6).format('DD/MM/YYYY HH:mm:ss');
                 End = moment().endOf('day').format('DD/MM/YYYY HH:mm:ss');
                 Label = "7 Hari Terakhir";
            }
            $('#dashboard-report-range').show();
        },


        //initAmChart1: function () {
        //    if (typeof(AmCharts) === 'undefined' || $('#dashboard_amchart_1').size() === 0) {
        //        return;
        //    }
        //    var chartData = [];
        //    var data = GetChartStagingMonth();
        //    if (data.length > 0) {
        //        for (i = 0; i < data.length; i++) {
        //            chartData.push({
        //                "date": moment(data[i].TANGGAL, '').format("YYYY-MM"),
        //                "data": data[i].JUMLAH,
        //                "townName": moment(data[i].TANGGAL, '').format("MMMM"),
        //            });
        //        }
        //    } 

        //    var chart = AmCharts.makeChart("dashboard_amchart_1", {
        //        type: "serial",
        //        fontSize: 12,
        //        fontFamily: "Open Sans",
        //        dataDateFormat: "YYYY-MM",
        //        dataProvider: chartData,

        //        addClassNames: true,
        //        startDuration: 1,
        //        color: "#6c7b88",
        //        marginLeft: 0,

        //        categoryField: "date",
        //        categoryAxis: {
        //            parseDates: true,
        //            minPeriod: "MM",
        //            autoGridCount: false,
        //            gridCount: 50,
        //            gridAlpha: 0.1,
        //            gridColor: "#FFFFFF",
        //            axisColor: "#555555",
        //            dateFormats: [{
        //                period: 'MM',
        //                format: 'MMM'
        //            }, {
        //                period: 'YYYY',
        //                format: 'YYYY'
        //            }]
        //        },

        //        valueAxes: [{
        //            id: "a1",
        //            title: "Jumlah Transaksi Tahun Ini",
        //            gridAlpha: 0,
        //            axisAlpha: 0
        //        }],
        //        graphs: [{
        //            id: "g1",
        //            valueField: "data",
        //            title: "Transaksi Berhasil",
        //            type: "column",
        //            fillAlphas: 0.7,
        //            valueAxis: "a1",
        //            balloonText: "[[value]] Transaksi",
        //            legendValueText: "[[value]] Transaksi",
        //            legendPeriodValueText: "Total: [[value.sum]] Transaksi",
        //            lineColor: "#32c5d2",
        //            alphaField: "alpha",
        //        }],

        //        chartCursor: {
        //            zoomable: false,
        //            categoryBalloonDateFormat: "MM",
        //            cursorAlpha: 0,
        //            categoryBalloonColor: "#e26a6a",
        //            categoryBalloonAlpha: 0.8,
        //            valueBalloonsEnabled: false
        //        },
        //        legend: {
        //            bulletType: "round",
        //            equalWidths: false,
        //            valueWidth: 120,
        //            useGraphSettings: true,
        //            color: "#6c7b88"
        //        }
        //    });
        //},

        //initAmChart2: function () {
        //    if (typeof(AmCharts) === 'undefined' || $('#dashboard_amchart_2').size() === 0) {
        //        return;
        //    }
        //    var chartData = [];
        //    var data = GetChartStagingYear();
        //    if (data.length >0) {
        //        for (i = 0; i < data.length; i++) {
        //            chartData.push({
        //                "date": data[i].TAHUN,
        //                "data": data[i].JUMLAH,
        //                "townName": data[i].TAHUN,
        //            });
        //        }
        //    }
            
        //    var chart = AmCharts.makeChart("dashboard_amchart_2", {
        //        type: "serial",
        //        fontSize: 12,
        //        fontFamily: "Open Sans",
        //        dataDateFormat: "YYYY",
        //        dataProvider: chartData,

        //        addClassNames: true,
        //        startDuration: 1,
        //        color: "#6c7b88",
        //        marginLeft: 0,

        //        categoryField: "date",
        //        categoryAxis: {
        //            parseDates: true,
        //            minPeriod: "YYYY",
        //            autoGridCount: false,
        //            gridCount: 50,
        //            gridAlpha: 0.1,
        //            gridColor: "#FFFFFF",
        //            axisColor: "#555555",
        //            dateFormats: [ {
        //                period: 'YYYY',
        //                format: 'YYYY'
        //            }]
        //        },

        //        valueAxes: [{
        //            id: "a1",
        //            title: "Jumlah Transaksi Beberapa Tahun Terakhir",
        //            gridAlpha: 0,
        //            axisAlpha: 0
        //        }],
        //        graphs: [{
        //            id: "g1",
        //            valueField: "data",
        //            title: "Transaksi Berhasil",
        //            type: "column",
        //            fillAlphas: 0.7,
        //            valueAxis: "a1",
        //            balloonText: "[[value]] Transaksi",
        //            legendValueText: "[[value]] Transaksi",
        //            legendPeriodValueText: "Total: [[value.sum]] Transaksi",
        //            lineColor: "#3598dc",
        //            alphaField: "alpha",
        //        }],

        //        chartCursor: {
        //            zoomable: false,
        //            categoryBalloonDateFormat: "YYYY",
        //            cursorAlpha: 0,
        //            categoryBalloonColor: "#e26a6a",
        //            categoryBalloonAlpha: 0.8,
        //            valueBalloonsEnabled: false
        //        },
        //        legend: {
        //            bulletType: "round",
        //            equalWidths: false,
        //            valueWidth: 120,
        //            useGraphSettings: true,
        //            color: "#6c7b88"
        //        }
        //    });
        //},

        init: function () {
            // Define first Date
            //PublicFunction();
            this.initDashboardDaterange();
            //GetDashboard(Start,End);
            //this.initAmChart1();
            //this.initAmChart2();
        }
    };

}();

jQuery(document).ready(function() {
    Dashboard.init(); // init metronic core componets
});
