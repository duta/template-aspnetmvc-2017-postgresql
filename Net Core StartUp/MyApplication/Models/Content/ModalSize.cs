﻿namespace MyApplication.Models
{
    public enum ModalSize
    {
        Small,
        Large,
        Medium
    }
}
