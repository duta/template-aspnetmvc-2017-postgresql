var MasterFunction = function () {

    var layout = function () {

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null
            };

            var url = window.location;
            $('.page-sidebar-menu li a').each(function () {
                if (this.pathname == url.pathname)
                {
                    $(this).parents('li').addClass('active open');
                    $(this).parent().addClass('active open');
                }
            });

            //var appInsights = window.appInsights || function (config) {
            //    function s(config) { t[config] = function () { var i = arguments; t.queue.push(function () { t[config].apply(t, i) }) } } var t = { config: config }, r = document, f = window, e = "script", o = r.createElement(e), i, u; for (o.src = config.url || "//az416426.vo.msecnd.net/scripts/a/ai.0.js", r.getElementsByTagName(e)[0].parentNode.appendChild(o), t.cookie = r.cookie, t.queue = [], i = ["Event", "Exception", "Metric", "PageView", "Trace"]; i.length;) s("track" + i.pop()); return config.disableExceptionTracking || (i = "onerror", s("_" + i), u = f[i], f[i] = function (config, r, f, e, o) { var s = u && u(config, r, f, e, o); return s !== !0 && t["_" + i](config, r, f, e, o), s }), t
            //}({
            //    instrumentationKey: "615aadc5-8508-46e7-aa93-713181a155ae"
            //});

            //window.appInsights = appInsights;
            //appInsights.trackPageView();
        }

        var Golobal = function () {

            $(".mask_date").inputmask("d/m/y", {
                "placeholder": "dd/mm/yyyy"
            });
            $(".mask_Telp").inputmask("mask", {
                "mask": "(999) 999-9999",
                rightAlignNumerics: true
            });
            $(".mask_Ktp").inputmask("mask", {
                "mask": "999-999-999-999-9999",
                rightAlignNumerics: true
            });
            $(".mask_Hp").inputmask("mask", {
                "mask": "(999) 999-999-999",
                rightAlignNumerics: true
            });
            $(".mask_Hp").inputmask("mask", {
                "mask": "99999999999999999",
                rightAlignNumerics: true
            });
            $(".mask_number").inputmask({
                "mask": "9",
                "repeat": 10,
                "greedy": false
            });
            $(".mask_decimal").inputmask('decimal', {
                rightAlignNumerics: true,
                placeholder: '0'
            });
            $('.mask_persen').inputmask('Regex',
                { regex: "^[1-9][0-9]?$|^100$" }
            );
            $('.mask_currency').inputmask("decimal", {
                radixPoint: ',',
                min: '1000',
                max: '1000000000000',
                placeholder: '0,00',
                groupSeparator: '.',
                digits: 2,
                digitsOptional: !1,
                autoGroup: true,
                prefix: ''
            });
            $('input.datetime').inputmask('datetime', { mask: "1/2/y", alias: "dd-mm-yyyy" });
        };

    return {

        init: function () {
            layout();
            Golobal();
        }
    };
}();

jQuery(document).ready(function () {
    MasterFunction.init();
});
