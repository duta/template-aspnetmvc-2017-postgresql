﻿using System;
using MyApplication.Entities;
using MyApplication.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Collections.Generic;
using SpinerService;
using System.ServiceModel;

namespace MyApplication.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var _Service = new PelindoServiceClient();

                // Using Service Spinner New Intergration To WS PORTAL SI
                var xLogin = await _Service.LoginAsync(new AuthRequest
                {
                    ID_APP = "103",
                    APP_NAME = "SPINER ULTIMATE",
                    USERNAME = model.UserName,
                    PASSWORD = model.Password
                });

                if (xLogin.Status == "S")
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, xLogin.Result.NAMA),
                        new Claim("ID_ROLE", xLogin.Result.ID_ROLE),
                        new Claim("ROLE", xLogin.Result.ROLE_NAME),
                        new Claim("EMAIL", xLogin.Result.EMAIL),
                        new Claim("ID_APP", xLogin.Result.ID_APP),
                        new Claim("APP_NAME", xLogin.Result.APP_NAME),
                        new Claim("KD_CABANG",  xLogin.Result.KD_CABANG),
                        new Claim("KD_TERMINAL", xLogin.Result.KD_TERMINAL),
                        // Validasi
                        new Claim("GUID", xLogin.Result.Validated.GUID),
                        new Claim("USERNAME", xLogin.Result.Validated.USERNAME),
                        new Claim("TOKEN", xLogin.Result.Validated.TOKEN)
                    };

                    var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                    await HttpContext.SignInAsync(
                        CookieAuthenticationDefaults.AuthenticationScheme,
                        new ClaimsPrincipal(claimsIdentity),
                        new AuthenticationProperties
                        {
                            IsPersistent = model.RememberMe,
                            ExpiresUtc = DateTime.UtcNow.AddMinutes(30)
                        }
                    );

                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, xLogin.Validasi.ToString());
                }
            }
            return View(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> SignOff()
        {
            await HttpContext.SignOutAsync(
                CookieAuthenticationDefaults.AuthenticationScheme);
            Response.Cookies.Delete("XSRF-TOKEN");
            return RedirectToAction("Login");
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(DashboardController.Index), "Dashboard");
            }
        }

        #region Error Handler

        public IActionResult AccessDenied()
        {
            return View("Error/403");

        }

        public IActionResult PageNotFound()
        {
            return View("Error/404");
        }

        public IActionResult InternalServerError()
        {
            return View("Error/500");
        }
        #endregion
    }
}
