﻿
namespace P3Apps.Models
{
    public enum ToastType
    {
        Error,
        Info,
        Success,
        Warning
    }
}