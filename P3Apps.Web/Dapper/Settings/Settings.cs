﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using P3Apps.Models;
using Dapper;
using System.Text;
using log4net;

namespace P3Apps.Dapper
{
    public class Settings : ISettings
    {
        private IDbConnection _db = new NpgsqlConnection(DatabaseHelper.GetConnectionString());
        private static readonly ILog _Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET MENU
        #region Menu
        public List<MenuModels> Menu(string id)
        {
            try
            {
                _Logger.Info("Using Razor Processing : Menu -> Success generate Recuresiving menu");

                if (id == "")
                {
                    string query = "SELECT * FROM TPSOL_MENU ORDER BY MENU_URUT ASC";
                    return this._db.Query<MenuModels>(query).ToList();
                }
                else {
                    string query = "SELECT A.* FROM TPSOL_MENU A, AKSES_MENU B WHERE MENU_ID = MENU_ID_KEY AND HAK_KODE_KEY = :hakkodekey ORDER BY MENU_URUT ASC";
                    return this._db.Query<MenuModels>(query, new { hakkodekey = id }).ToList();
                }
            }
            catch (Exception e)
            {
                _Logger.Fatal("Using Razor Processing : Menu -> " + e.Message.ToString());
                return null;
            }
        }
        public string Menu_Store(MenuModels request)
        {
            try
            {
                string query = "INSERT INTO TPSOL_MENU (MENU_ID,MENU_NAMA,MENU_URL,MENU_ALIAS,MENU_PARENT_ID,MENU_ICON,MENU_URUT) VALUES (TPSOL_SQ_MENU.nextval,:A,:B,:C,:D,:E,:F)";
                this._db.Query<MenuModels>(query, new { A = request.Menu_Nama, B = request.Menu_Url, C = request.Menu_Alias, D = "", E = request.Menu_Icon, F = request.Menu_URUT });
                return "S";
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }
        public List<MenuModels> Menu_Edit(string id)
        {
            try
            {
                string query = "SELECT * FROM TPSOL_MENU WHERE MENU_ID = :id";
                return this._db.Query<MenuModels>(query, new { id = id }).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public string Menu_Update(string id, MenuModels request)
        {
            try
            {
                 string query = "UPDATE TPSOL_MENU SET MENU_NAMA = :A, MENU_URL = :B, MENU_ALIAS = :C, MENU_ICON = :E WHERE MENU_ID = :id";
                 this._db.Query<MenuModels>(query, new { A = request.Menu_Nama, B = request.Menu_Url, C = request.Menu_Alias, E = request.Menu_Icon, id = id });
                 return "S";
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }
        public string Menu_Destroy(string id)
        {
            try
            {
                string query = "DELETE FROM TPSOL_MENU WHERE MENU_ID = :id";
                this._db.Query<MenuModels>(query, new { id = id });

                string query2 = "DELETE FROM AKSES_MENU WHERE MENU_ID_KEY = :id";
                this._db.Query<AksesMenuModels>(query2, new { id = id });

                return "S";
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }
        public string Menu_StoreOrderMenu(IEnumerable<OrderMenuModels> Request)
        {
            try
            {
                string query = "UPDATE TPSOL_MENU SET MENU_PARENT_ID = :A, MENU_URUT =:C WHERE MENU_ID = :B";

                foreach (var item in Request) {
                    this._db.Query<OrderMenuModels>(query, new { A = item.parentid, C = item.urutan, B = item.id });
                }

                return "S";
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }
        #endregion

        // GET HAK AKSES
        #region Hak Akses
        public List<HakAksesModels> HakAkses()
        {
            try
            {
                string query = "SELECT * FROM TPSOL_HAK_AKSES ORDER BY HAK_ID ASC";
                return this._db.Query<HakAksesModels>(query).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public IEnumerable<HakAksesModels> HakAkses_GetHakAkses(string Select, string Where, string Limit, string OrderBy)
        {
            try
            {
                string query = "";
                if (Where != "")
                {
                    query = "SELECT * FROM (SELECT  " + Select + " , ROW_NUMBER() OVER (ORDER BY " + OrderBy + ") R FROM TPSOL_HAK_AKSES WHERE " + Where + ") WHERE " + Limit;
                }
                else
                {
                    query = "SELECT * FROM (SELECT  " + Select + " , ROW_NUMBER() OVER (ORDER BY " + OrderBy + ") R FROM TPSOL_HAK_AKSES) WHERE " + Limit;
                }
                return this._db.Query<HakAksesModels>(query).ToList();
            }
            catch (Exception)
            {
                return null;
            }

        }

        public string HakAkses_Store(HakAksesModels request)
        {
            try
            {
                string query = "INSERT INTO TPSOL_HAK_AKSES (HAK_ID,HAK_KODE,HAK_NAMA) VALUES (TPSOL_SQ_HAKAKSES.nextval,:A,:B)";
                this._db.Query<HakAksesModels>(query, new { A = request.Hak_Kode, B = request.Hak_Nama });
                return "S";
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        public string HakAkses_Destroy(string id)
        {
            try
            {
                string query = "DELETE FROM TPSOL_HAK_AKSES WHERE HAK_KODE = :id";
                this._db.Query<HakAksesModels>(query, new { id = id });

                string query2 = "DELETE FROM AKSES_MENU WHERE HAK_KODE_KEY = :id";
                this._db.Query<AksesMenuModels>(query2, new { id = id });

                return "S";
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        public List<HakAksesModels> HakAkses_Edit(string id)
        {
            try
            {
                string query = "SELECT * FROM TPSOL_HAK_AKSES WHERE HAK_ID = :id ORDER BY HAK_ID ASC";
                return this._db.Query<HakAksesModels>(query, new { id = id }).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string HakAkses_Update(string id, HakAksesModels Request)
        {
            try
            {
                string query = "UPDATE TPSOL_HAK_AKSES SET HAK_KODE = :A, HAK_NAMA = :B WHERE HAK_KODE = :id";
                this._db.Query<HakAksesModels>(query, new { A = Request.Hak_Kode, B = Request.Hak_Nama, id = id });

                string query2 = "UPDATE AKSES_MENU SET HAK_KODE_KEY = :A WHERE HAK_KODE_KEY = :id";
                this._db.Query<AksesMenuModels>(query2, new { A = Request.Hak_Kode, id = id });

                return "S";
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }
        public string HakAkses_StoreAkses(IEnumerable<AksesMenuModels> Request)
        {
            try
            {
                var items = Request.GroupBy(m => m.HAK_KODE_KEY);
                string query = "DELETE FROM AKSES_MENU WHERE HAK_KODE_KEY = :C";
                foreach (var item in items)
                {
                    this._db.Query<AksesMenuModels>(query, new { C = Encoding.UTF8.GetString(Convert.FromBase64String(item.Key)) });
                }

                var items2 = Request.Where(m => m.MENU_ID_KEY != null);
                string query2 = "INSERT INTO AKSES_MENU (HAK_KODE_KEY,MENU_ID_KEY) VALUES (:A,:B)";
                foreach (AksesMenuModels item in items2)
                {
                    this._db.Query<AksesMenuModels>(query2, new { A = Encoding.UTF8.GetString(Convert.FromBase64String(item.HAK_KODE_KEY)), B = Encoding.UTF8.GetString(Convert.FromBase64String(item.MENU_ID_KEY)) });
                }

                return "S";
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }
        public List<AksesMenuModels> HakAkses_AksesMenu(string id)
        {
            try
            {
                string query = "SELECT * FROM AKSES_MENU WHERE HAK_KODE_KEY = :hakkodekey ORDER BY HAK_KODE_KEY ASC";
                return this._db.Query<AksesMenuModels>(query, new { hakkodekey = id }).ToList();
            }
            catch (Exception)
            {
                return  null;
            }
        }
        #endregion
    }
}