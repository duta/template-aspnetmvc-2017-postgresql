﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace MyApplication.Entities
{
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole()
        {
        }

        public ApplicationRole(string name)
           : this()
        {
            this.Name = name;
            MenuItems = new HashSet<ApplicationRoleMenu>();

        }

        public ICollection<ApplicationRoleMenu> MenuItems { get; set; }
        public virtual IdentityUserRole<string> User { get; set; }

    }
}
