﻿using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using static Microsoft.AspNetCore.ResponseCompression.ResponseCompressionDefaults;

namespace MyApplication.Entities
{
    public static class Helpers
    {
        public static IEnumerable<string> DefaultMimeTypes => MimeTypes.Concat(new[]
        {
            "image/svg+xml",
            "application/font-woff2"
        });
    }
}