﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using P3Apps.Models;
using P3Apps.Dapper;
using System.Text;
using log4net;

namespace P3Apps.Api
{
    public class GlobalController : ApiController
    {
        // GlobalController Untuk API, Untuk WS
        private IGlobalHelper _GlobalHelper = new GlobalHelper();
        private static readonly ILog _Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //[HttpGet, PelindoValidateAntiForgeryToken]
        //public IEnumerable<MasterPelabuhan> GetAllOrPelabuhanById(string Request)
        //{
        //    IEnumerable<MasterPelabuhan> Respose = null;
        //    try
        //    {
        //        if (User.Identity.IsAuthenticated)
        //        {
        //            var claimsIdentity = User.Identity as ClaimsIdentity;
        //            string
        //                KD_CABANG = claimsIdentity.FindFirst("KD_CABANG").Value;

        //            // DECODE BASE64
        //            var RequestDecodeBytes = Convert.FromBase64String(Request);
        //            string RequestDecoded = Encoding.UTF8.GetString(RequestDecodeBytes);

        //            Respose = _GlobalHelper.GetAllOrPelabuhanById(RequestDecoded.ToUpper(), KD_CABANG, "Like");

        //            if (Respose.Count() > 0)
        //            {
        //                _Logger.Info("By " + claimsIdentity.FindFirst("USERNAME").Value + " - Using API : GetAllOrPelabuhanById -> Success data Pelabuhan di return");
        //            }
        //            else
        //            {
        //                _Logger.Fatal("By " + claimsIdentity.FindFirst("USERNAME").Value + " - Using API : GetAllOrPelabuhanById -> Error Exception, Terjadi Kesalahan Query");
        //            }
        //        }
        //        else
        //        {
        //            _Logger.Error("Using API : GetAllOrPelabuhanById -> Mohon maaf permintaan kami tolak karena anda belum login");
        //        }
        //        return Respose;
        //    }
        //    catch (Exception e)
        //    {
        //        _Logger.Fatal("Using API : GetAllOrPelabuhanById -> " + e.Message.ToString());
        //        return Respose;
        //    }
        //}
    }
}