﻿namespace MyApplication.Models
{
    public class ModalFooter
    {
        public string SubmitButtonText { get; set; } = "Save";
        public string EraseButtonText { get; set; } = "Erase";
        public string CancelButtonText { get; set; } = "Cancel";
        public string SubmitButtonID { get; set; } = "btn-submit";
        public string EraseButtonID { get; set; } = "btn-erase";
        public string CancelButtonID { get; set; } = "btn-cancel";
        public bool OnlyCancelButton { get; set; } = false;
        public bool OnlyCancelSaveButton { get; set; } = false;
    }
}