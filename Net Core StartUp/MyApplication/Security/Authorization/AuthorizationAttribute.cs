﻿using System.Threading.Tasks;
using MyApplication.Entities;
using MyApplication.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Data;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace MyApplication.Security.Authorization
{
    public class MyAuthorizeAttribute : TypeFilterAttribute
    {
         public MyAuthorizeAttribute(string Role, string Menu, AuthPermissions Permis) : base(typeof(AuthorizeActionFilter))
         {
            Arguments = new object[] { Role, Menu, Permis };
         }
    }

    public class AuthorizeActionFilter : IAsyncActionFilter
    {
        private readonly string _Role;
        private readonly string _Menu;
        private readonly AuthPermissions _Permis;
        private ApplicationDbContext _Context;
        //private IDbConnection _db = new NpgsqlConnection(DbHelper.GetConnectionString());

        public AuthorizeActionFilter(ApplicationDbContext context,string Role, string Menu, AuthPermissions Permis)
        {
            _Role = Role;
            _Menu = Menu;
            _Permis = Permis;
            _Context = context;
        }
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            bool isAuthorized = await AuthorizeAsync(context.HttpContext.User, _Role, _Menu, _Permis);

            if (!isAuthorized)
            {
                context.Result = new ForbidResult();
            }
            else
            {
                await next();
            }
        }

        private Task<bool> AuthorizeAsync(ClaimsPrincipal user, string role, string menu, AuthPermissions permis)
        {
            if (user.Identity.IsAuthenticated)
            {
                bool HasRole = user.IsInRole(role.ToString());

                var model = new MenuViewModel();
                var MenuManager = new MenuManager(_Context);

                model.MenuItems = MenuManager.GetAllByUser(user.Identity.Name).ToList();

                if (model.MenuItems.Count() != 0 && HasRole)
                {
                    HasRole = true;
                }
                else
                {
                    HasRole = false;
                };
                return Task.FromResult(HasRole);
            }
            else
            {
                return Task.FromResult(false);
            }
        }
    }
    public enum AuthPermissions
    {
        Read,  Create, Edit, View, Update, Delete
    }
}