﻿var MyAplication = function () {
    //var listsd = { _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) { var t = ""; var n, r, i, s, o, u, a; var f = 0; e = listsd._utf8_encode(e); while (f < e.length) { n = e.charCodeAt(f++); r = e.charCodeAt(f++); i = e.charCodeAt(f++); s = n >> 2; o = (n & 3) << 4 | r >> 4; u = (r & 15) << 2 | i >> 6; a = i & 63; if (isNaN(r)) { u = a = 64 } else if (isNaN(i)) { a = 64 } t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a) } return t }, decode: function (e) { var t = ""; var n, r, i; var s, o, u, a; var f = 0; e = e.replace(/[^A-Za-z0-9+/=]/g, ""); while (f < e.length) { s = this._keyStr.indexOf(e.charAt(f++)); o = this._keyStr.indexOf(e.charAt(f++)); u = this._keyStr.indexOf(e.charAt(f++)); a = this._keyStr.indexOf(e.charAt(f++)); n = s << 2 | o >> 4; r = (o & 15) << 4 | u >> 2; i = (u & 3) << 6 | a; t = t + String.fromCharCode(n); if (u != 64) { t = t + String.fromCharCode(r) } if (a != 64) { t = t + String.fromCharCode(i) } } t = listsd._utf8_decode(t); return t }, _utf8_encode: function (e) { e = e.replace(/rn/g, "n"); var t = ""; for (var n = 0; n < e.length; n++) { var r = e.charCodeAt(n); if (r < 128) { t += String.fromCharCode(r) } else if (r > 127 && r < 2048) { t += String.fromCharCode(r >> 6 | 192); t += String.fromCharCode(r & 63 | 128) } else { t += String.fromCharCode(r >> 12 | 224); t += String.fromCharCode(r >> 6 & 63 | 128); t += String.fromCharCode(r & 63 | 128) } } return t }, _utf8_decode: function (e) { var t = ""; var n = 0; var r = c1 = c2 = 0; while (n < e.length) { r = e.charCodeAt(n); if (r < 128) { t += String.fromCharCode(r); n++ } else if (r > 191 && r < 224) { c2 = e.charCodeAt(n + 1); t += String.fromCharCode((r & 31) << 6 | c2 & 63); n += 2 } else { c2 = e.charCodeAt(n + 1); c3 = e.charCodeAt(n + 2); t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63); n += 3 } } return t } };
    var XLOA = ""; var LOA = 0 ; var XGt = ""; var XBendera = ""; var XPalka = ""; var XPalkaType = "";

    var init = function () {

        //$.urlParam = function (name) {
        //    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
        //    return (results !== null) ? results[1] || 0 : "";
        //}

        //if ($.urlParam('srv') != '') { service = listsd.decode($.urlParam('srv')); }

        $('.datetimepicker').datetimepicker({
            format: 'DD/MM/YYYY HH:mm'
        });


        $(".maskNumeric").inputmask({
            alias: 'numeric',
            allowMinus: false,
            digits: 0,
            placeholder: '0',
            max: 9999
        });

        $('#erase').on('click', function () {
            swal({
                title: 'Are you sure?',
                text: "You won't be erase this form!",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, erase it!'
            }).then((result) => {
                if (result.value) {
                    Erase();
                    swal(
                        'Erase!',
                        'Your form has been erased.',
                        'success'
                    )
                }
            })
        });

        $('#save').on("click", function () {
            swal({
                title: 'Are you sure?',
                text: "You won't be save this!",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, save it!'
            }).then((result) => {
                if (result.value) {
                    SaveVesselScheduleEstimate("/VesselPlanning/VesselScheduleEstimate/Create");
                }
            })
        });

        $("[name=ZKdFrom]").keyup(function () {
            var from = parseInt($(this).val()) ? parseInt($(this).val()) : 0;
            $("[name=ZKdTo]").val(from + parseInt(LOA));
        });
    };

    var GetVessel = function () {

        function formatRepo(repo) {
            if (repo.loading) return repo.text;
            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'>" + repo.mkpl_nama + " (" + repo.id + ") </div>" +
                "<div class='select2-result-repository__description'>Gross Tonnage (GT): " + repo.mkpl_grt + " / Length Overall (LOA): " + repo.mkpl_loa + "</div></div>";
            return markup;
        }

        function formatRepoSelection(repo) {
            return typeof repo.mkpl_nama !== 'undefined' ? repo.mkpl_nama + " - (" + repo.id + ") "  : repo.text;
        }

        $("[name=vessel]").select2({
            allowClear: true,
            width: "off",
            ajax: {
               // type: "POST",
                url: "/GlobalService/GetVessel",
                dataType: 'json',
                data: function (params) {
                    return {
                        Request: params.term.toUpperCase()
                    };
                },
                headers: { 'X-XSRF-TOKEN': app.AntiforgeryToken },
                processResults: function (data) {
                    var x = $.grep(data.result, function (item) {
                        var str = item.mkpl_kode.toUpperCase() + item.mkpl_nama.toUpperCase();
                        item.id = item.mkpl_kode.toUpperCase();
                        item.text = item.mkpl_nama.toUpperCase() + ' - ' + item.mkpl_kode.toUpperCase();
                        var patt = new RegExp($('input.select2-search__field').val().toUpperCase());
                        return patt.test(str);
                    });
                    return {
                        results: x
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection,
            placeholder: "Tekan enter lalu ketikkan nama atau kode Kapal"
        }).on("select2:select", function (e) {
            XLOA = e.params.data.mkpl_loa;
            XGt = e.params.data.mkpl_grt;
            XBendera = e.params.data.mkpl_bendera;
            XPalka = e.params.data.mkpl_palka;
            XPalkaType = e.params.data.mkpl_jns_palka;
            $("[name=ZKdFrom]").val("0");
            LOA = e.params.data.mkpl_loa;
            $("[name=ZKdTo]").val(e.params.data.mkpl_loa);
        }).on("select2:unselecting", function (e) {
            XLOA = "";
            XGt = "";
            XBendera = "";
            XPalka = "";
            XPalkaType = "";
            $("[name=ZKdFrom]").val("0");
            $("[name=ZKdTo]").val("0");
            LOA = 0;
        }).trigger('change');
    };

    var GetPelabuhan = function () {

        function formatRepo(repo) {
            if (repo.loading) return repo.text;
            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'>" + repo.mplb_nama + "</div>" +
                "<div class='select2-result-repository__description'>Kode : " + repo.id + " / Kota : " + repo.mplb_kota + " </div></div>";
            return markup;
        }

        function formatRepoSelection(repo) {
            return typeof repo.mplb_nama !== 'undefined' ? repo.mplb_nama  : repo.text;
        }

        $("[name=origin], [name=next], [name=prev], [name=dest]").select2({
            allowClear: true,
            width: "off",
            ajax: {
                url: "/GlobalService/GetPelabuhan",
                dataType: 'json',
                data: function (params) {
                    return {
                        Request: params.term.toUpperCase()
                    };
                },
                headers: { 'X-XSRF-TOKEN': app.AntiforgeryToken },
                processResults: function (data) {
                    var x = $.grep(data.result, function (item) {
                        var str = item.mplb_kode.toUpperCase() + item.mplb_nama.toUpperCase();
                        item.id = item.mplb_kode.toUpperCase();
                        item.text = item.mplb_nama.toUpperCase();
                        var patt = new RegExp($('input.select2-search__field').val().toUpperCase());
                        return patt.test(str);
                    });

                    return {
                        results: x
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection,
            placeholder: "Ketikkan nama atau kode Pelabuhan"
        });
    };

    var GetDermaga = function () {

        function formatRepo(repo) {
            if (repo.loading) return repo.text;
            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'>" + repo.mdmg_nama +"</div>" +
                "<div class='select2-result-repository__description'>Kode : " + repo.id + " / Jenis Dermaga : " + repo.mdmg_jenis_dmg + " </div></div>";
            return markup;
        }

        function formatRepoSelection(repo) {
            return typeof repo.mdmg_nama !== 'undefined' ? repo.mdmg_nama : repo.text;
        }

        $("[name=wharfag]").select2({
            allowClear: true,
            width: "off",
            ajax: {
                url: "/GlobalService/GetDermaga",
                dataType: 'json',
                data: function (params) {
                    return {
                        Request: params.term.toUpperCase()
                    };
                },
                headers: { 'X-XSRF-TOKEN': app.AntiforgeryToken },
                processResults: function (data) {
                    var x = $.grep(data.result, function (item) {
                        var str = item.mdmg_kode.toUpperCase() + item.mdmg_nama.toUpperCase();
                        item.id = item.mdmg_kode.toUpperCase();
                        item.text = item.mdmg_nama.toUpperCase();
                        var patt = new RegExp($('input.select2-search__field').val().toUpperCase());
                        return patt.test(str);
                    });

                    return {
                        results: x
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection,
            placeholder: "Ketikkan nama atau kode Dermaga"
        });
    };

    var GetPelanggan = function () {

        function formatRepo(repo) {
            if (repo.loading) return repo.text;
            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__title'>" + repo.mplG_NAMA + "</div>" +
                "<div class='select2-result-repository__description'>Kode : " + repo.id + " / Kota : " + repo.mplG_KOTA + " / Alamat : " + repo.mplG_ALAMAT + " </div></div>";
            return markup;
        }

        function formatRepoSelection(repo) {
            return typeof repo.mplG_NAMA !== 'undefined' ? repo.mplG_NAMA : repo.text;
        }

        $("[name=agen]").select2({
            allowClear: true,
            width: "off",
            ajax: {
                url: "/GlobalService/GetPelanggan",
                dataType: 'json',
                data: function (params) {
                    return {
                        Request: params.term.toUpperCase()
                    };
                },
                headers: { 'X-XSRF-TOKEN': app.AntiforgeryToken },
                processResults: function (data) {
                    var x = $.grep(data.result, function (item) {
                        var str = item.mplG_KODE.toUpperCase() + item.mplG_NAMA.toUpperCase();
                        item.id = item.mplG_KODE.toUpperCase();
                        item.text = item.mplG_NAMA.toUpperCase();
                        var patt = new RegExp($('input.select2-search__field').val().toUpperCase());
                        return patt.test(str);
                    });

                    return {
                        results: x
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 3,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection,
            placeholder: "Ketikkan nama atau kode agen"
        });
    };

    var GetDataVessel = function () {
        var table = $('#MyDatatable')
        table.DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/VesselPlanning/VesselScheduleEstimate/GetDataVessel",
                "type": "POST",
                "headers": {
                    'X-XSRF-TOKEN': app.AntiforgeryToken
                },
            },
            "deferRender": true,
            "columns": [
                { "data": "veS_CODE", "name": "veS_CODE", "autoWidth": true  },
                { "data": "veS_NAME", "name": "veS_NAME", "autoWidth": true  },
                { "data": "mplG_NAMA", "name": "mplG_NAMA", "autoWidth": true  },
                {
                    "data": "esT_ARRIVAL_TIME", "name": "esT_ARRIVAL_TIME", "autoWidth": true,
                    "render": function (data, type, full, meta) {
                        return moment(data, 'DD/MM/YYYY HH:mm').format("DD MMMM YYYY HH:mm");
                    }
                },
                {
                    "data": "esT_DEPARTURE_TIME", "name": "esT_DEPARTURE_TIME", "autoWidth": true,
                    "render": function (data, type, full, meta) {
                        return moment(data, 'DD/MM/YYYY HH:mm').format("DD MMMM YYYY HH:mm");
                    }
                },
                { "data": "lokasi", "name": "lokasi", "autoWidth": true },
                { "data": "lasT_PORT_NAME", "name": "lasT_PORT_NAME", "autoWidth": true },
                { "data": "nexT_PORT_NAME", "name": "nexT_PORT_NAME", "autoWidth": true },
                { "data": "origiN_PORT_NAME", "name": "origiN_PORT_NAME", "autoWidth": true },
                { "data": "desT_PORT_NAME", "name": "desT_PORT_NAME", "autoWidth": true },
                {
                    "data": null, "autoWidth": true,
                    "render": function (data, type, full, meta) {
                        return full.gt + ' - ' + full.l_O_A;
                    }
                },
                {
                    "data": null, "autoWidth": true,
                    "render": function (data, type, full, meta) {
                        return full.kD_FROM + ' - ' + full.kD_TO;

                    }
                },
                {
                    "data": 'veS_CODE', "name": 'action', "autoWidth": true,
                    "render": function (data, type, full, meta) {
                        var html = '<a class="btn btn-xs btn-primary edit" title="Edit"><i class="fa fa-edit"></i></a>' +
                            '<button type="button" onclick="" class="btn btn-xs btn-danger" title="Hapus"><i class="fa fa-trash"></i></button></div>';
                        return html;
                    }
                },
            ],
            "columnDefs": [
                {   
                    'orderable': false,
                    "searchable": false,
                    'targets': [6,7]
                }
            ],
            "order": [
                [1, "desc"]
            ],
            "lengthMenu": [
                [5, 10, 15, 20, 50, 100],
                [5, 10, 15, 20, 50, 100]  
            ],
            "pageLength": 10,
            "responsive": {
                "details": {
                    "display": $.fn.dataTable.Responsive.display.modal({
                        "header": function (row) {
                            var data = row.data();
                            return 'Details for ' + data.veS_NAME;
                        },
                        "footer": function (row) {
                            var data = row.data();
                            //return html = '<button type="button" class="btn btn-bold btn-pure text-white" data-dismiss="modal"><i class="ti-pencil"></i> Edit</button>' +
                            //              '<button type="button" class="btn btn-bold btn-pure text-white" data-dismiss="modal"><i class="ti-trash"></i> Delete</button>';
                        }
                    }),
                    //"renderer": $.fn.dataTable.Responsive.renderer.listHiddenNodes({
                    //    tableClass: 'table'
                    //})
                    "renderer": function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col) {
                            if (col.columnIndex !== 12) {
                                return col.hidden ? '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                    '<td>' + col.title + ' : ' + '</td> ' +
                                    '<td>' + col.data + '</td>' +
                                    '</tr>' : '';
                            }
                        }).join('');
                        return $('<table class="table dtr-details" width="100%"/>').append(data);
                    }
                }
            }
        });

        //table.on('change', 'tbody tr', function () {
        //    jQuery("tr").each(function () {
        //        if (jQuery(this).hasClass("active")) {
        //            $(this).removeClass("active");
        //        }
        //    });
        //    $(this).addClass("active");
        //});
    };

    var SaveVesselScheduleEstimate = function (url) {

        App_BlockUI("Menyimpan Data...", 0);

        var MyObj = {
            "XVesCode": $("[name=vessel] option:selected").val(),
            "XVesName": $("[name=vessel] option:selected").text().split(" -")[0],
            "XAgent": $("[name=agen] option:selected").val(),
            "XPBM": "",
            "XLOA": XLOA ? XLOA : 0,
            "XKeterangan": $("[name=remark]").val(),
            "ZKdFrom": $("[name=ZKdFrom]").val(),
            "ZKdTo": $("[name=ZKdTo]").val(),
            "XEstArrivalTime": $("[name=arrival]").val(),
            "XEstDepartureTime": $("[name=departure]").val(),
            "XLastPort": $("[name=prev] option:selected").val(),
            "XNextPort": $("[name=next] option:selected").val(),
            "XOriginPort": $("[name=origin] option:selected").val(),
            "XDestPort": $("[name=dest] option:selected").val(),
            "XLastPortName": $("[name=prev] option:selected").text(),
            "XNextPortName": $("[name=next] option:selected").text(),
            "XOriginPortName": $("[name=origin] option:selected").text(),
            "XDestPortName": $("[name=dest] option:selected").text(),
            "XBerthNo": $("[name=wharfag] option:selected").val(),
            "XBerthName": $("[name=wharfag] option:selected").text(),
            "XGt": XGt ? XGt : 0,
            "XBendera": XBendera ? XBendera : "",
            "XPalka": XPalka ? XPalka : 0,
            "XPalkaType": XPalkaType ? XPalkaType : ""
        };

        //console.log(MyObj);
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(MyObj),
            contentType: 'application/json; charset=utf-8',
            headers: { 'X-XSRF-TOKEN': app.AntiforgeryToken },
            success: function (data) {
                if (data.status === "S") {
                    App_UnblockUI();
                    //toastr.success(data.message, 'Saved Successfully');
                    swal('Saved Successfully!', data.message, 'success');
                    Erase();
                } else if (data.status === "ET") {
                    App_UnblockUI();
                 //   toastr.error(data.message, 'Error Token');
                    swal('Error Token!', data.message, 'error');
                } else if (data.status === "EV") {
                    App_UnblockUI();
                   // toastr.error(data.message, 'Error Validation');
                    swal('Error Validation!', data.message, 'error');
                } else if (data.status === "EX") {
                    App_UnblockUI();
                   // toastr.error(data.message, 'Error Exception');
                    swal('Error Exception!', data.message, 'error');
                } else if (data.status === "E") {
                    App_UnblockUI();
                    //toastr.error(data.message, 'Saved Failed');
                    swal('Saved Failed!', data.message, 'error');
                }else if (data.status === "401") {
                    App_UnblockUI();
                    //toastr.error(data.message, 'Saved Failed');
                    swal('Saved Failed! 401', data.message, 'error');
                }
            },
            error: function (data) {
                App_UnblockUI();
                toastr.error('Saving Vessel Schedule Estimate Flure!.', 'Fatal Error'); 
                swal('Fatal Error!', 'Saving Vessel Schedule Estimate Flure!.', 'error');
            }
        });
    };

    var Erase = function () {
        document.getElementById("create").reset();
        $('select.select2').val('1').trigger('change');
    };

    return {
        init: function () {
            init();
            GetVessel();
            GetPelabuhan();
            GetDermaga();
            GetPelanggan();
            GetDataVessel();
            Erase();
        }
    };

}();

jQuery(document).ready(function () {
    MyAplication.init();
});