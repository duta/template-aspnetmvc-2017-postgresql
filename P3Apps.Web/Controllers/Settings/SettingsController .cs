﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;
using P3Apps.Dapper;
using P3Apps.Models;
using System.Text;
using log4net;

namespace P3Apps.Controllers
{
    public class SettingsController : Controller
    {
        private ISettings _Settings = new Settings();
        private IGlobalHelper _GlobalHelper = new GlobalHelper();
        private static readonly ILog _Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        // GET: Menu
        #region Menu
        public ActionResult Menu()
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    string
                        username = claimsIdentity.FindFirst("USERNAME").Value,
                        hakakses = claimsIdentity.FindFirst("HAKAKSES").Value;
                    ViewBag.USERNAME = username;
                    ViewBag.HAKAKSES = hakakses;

                    _Logger.Info("By " + username + " : Menu -> Success returning to Menu/Index");
                    return View("Menu/Index");
                }
                else
                {
                    _Logger.Error("Menu -> Mohon maaf permintaan kami tolak karena anda belum login");
                   return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
                }
            }
            catch (Exception e)
            {
                _Logger.Fatal("Menu -> " + e.Message.ToString());
               return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
            }
        }

        [PelindoValidateAntiForgeryToken]
        public ActionResult Menu_Store(MenuModels Requests)
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    if (ModelState.IsValid)
                    {
                        var claimsIdentity = User.Identity as ClaimsIdentity;
                        string
                            username = claimsIdentity.FindFirst("USERNAME").Value,
                            hakakses = claimsIdentity.FindFirst("HAKAKSES").Value;

                        ViewBag.USERNAME = username;
                        ViewBag.HAKAKSES = hakakses;

                        var Response = _Settings.Menu_Store(Requests);

                        if (Response == "S")
                        {
                            _Logger.Info("By " + username + " : Menu_Store -> Data berhasil di simpan");
                            this.AddToastMessage("Sukses", "Data berhasil di simpan", ToastType.Success);
                        }
                        else
                        {
                            _Logger.Fatal("By " + username + " : Menu_Store -> " + Response);
                            this.AddToastMessage("Error Query", "Terjadi kesalahan di pengambilan data", ToastType.Error);
                        }
                    }
                    else
                    {
                        _Logger.Warn("Menu_Store -> Paremeter tidak valid");
                        this.AddToastMessage("Error Parameter", "Paremeter tidak valid", ToastType.Error);
                    }
                }
                else
                {
                    _Logger.Error("Menu_Store -> Mohon maaf permintaan kami tolak karena anda belum login");
                    this.AddToastMessage("Error Login", "Mohon maaf permintaan kami tolak karena anda belum login", ToastType.Error);
                }
                return RedirectToAction("Menu", "Settings");
            }
            catch (Exception e)
            {
                _Logger.Fatal("Menu_Store -> "+ e.Message.ToString());
                this.AddToastMessage("Error", e.Message.ToString(), ToastType.Error);
               return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
            }
        }

        public ActionResult Menu_Edit(string id)
        {
            List<MenuModels> MyMenu = null;
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    string
                        username = claimsIdentity.FindFirst("USERNAME").Value,
                        hakakses = claimsIdentity.FindFirst("HAKAKSES").Value;
                    ViewBag.USERNAME = username;
                    ViewBag.HAKAKSES = hakakses;

                    var DecodeBytes = Convert.FromBase64String(id);
                    string Decoded = Encoding.UTF8.GetString(DecodeBytes);

                    ViewBag.Menu_ID = id;
                    MyMenu = _Settings.Menu_Edit(Decoded);

                    if (MyMenu.Count() > 0)
                    {
                        _Logger.Info("By " + username + " : Menu_Edit -> Success returning data Menu_Edi");
                    }
                    else
                    {
                        _Logger.Fatal("By " + username + " : Menu_Edit -> Error Exception, Terjadi Kesalahan Query");
                    }
                    return View("Menu/Index", MyMenu);
                }
                else
                {
                    _Logger.Error("Menu_Edit -> Mohon maaf permintaan kami tolak karena anda belum login");
                   return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
                }
            }
            catch (Exception e)
            {
                _Logger.Fatal("Menu_Edit -> " + e.Message.ToString());
               return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
            }
        }

        [PelindoValidateAntiForgeryToken]
        public ActionResult Menu_Update(string id, MenuModels Requests)
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    if (ModelState.IsValid)
                    {
                        var claimsIdentity = User.Identity as ClaimsIdentity;
                        string
                            username = claimsIdentity.FindFirst("USERNAME").Value,
                            hakakses = claimsIdentity.FindFirst("HAKAKSES").Value;

                        ViewBag.USERNAME = username;
                        ViewBag.HAKAKSES = hakakses;

                        var DecodeBytes = Convert.FromBase64String(id);
                        string Decoded = Encoding.UTF8.GetString(DecodeBytes);

                        var Response = _Settings.Menu_Update(Decoded, Requests);

                        if (Response == "S")
                        {
                            _Logger.Info("By " + username + " : Menu_Update -> Data berhasil di ubah");
                            this.AddToastMessage("Sukses", "Data berhasil di update", ToastType.Success);
                        }
                        else
                        {
                            _Logger.Fatal("By " + username + " : Menu_Update -> " + Response);
                            this.AddToastMessage("Error Query", "Terjadi kesalahan di pengambilan data", ToastType.Error);
                        }
                    }
                    else
                    {
                        _Logger.Warn("Menu_Update -> Paremeter tidak valid");
                        this.AddToastMessage("Error Parameter", "Paremeter tidak valid", ToastType.Error);
                    }
                }
                else
                {
                    _Logger.Error("Menu_Update -> Mohon maaf permintaan kami tolak karena anda belum login");
                    this.AddToastMessage("Error Login", "Mohon maaf permintaan kami tolak karena anda belum login", ToastType.Error);
                }
                return RedirectToAction("Menu", "Settings");
            }
            catch (Exception e)
            {
                _Logger.Fatal("Menu_Update -> " + e.Message.ToString());
                this.AddToastMessage("Error", e.Message.ToString(), ToastType.Error);
               return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
            }
        }

        [PelindoValidateAntiForgeryToken]
        public ActionResult Menu_Destroy(string id)
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    if (ModelState.IsValid)
                    {
                        var claimsIdentity = User.Identity as ClaimsIdentity;
                        string
                            username = claimsIdentity.FindFirst("USERNAME").Value,
                            hakakses = claimsIdentity.FindFirst("HAKAKSES").Value;

                        ViewBag.USERNAME = username;
                        ViewBag.HAKAKSES = hakakses;

                        var DecodeBytes = Convert.FromBase64String(id);
                        string Decoded = Encoding.UTF8.GetString(DecodeBytes);

                        var Response = _Settings.Menu_Destroy(Decoded);

                        if (Response == "S")
                        {
                            _Logger.Info("By " + username + " : Menu_Destroy -> Data berhasil di hapus");
                            this.AddToastMessage("Sukses", "Data berhasil di hapus", ToastType.Success);
                        }
                        else
                        {
                            _Logger.Fatal("By " + username + " : Menu_Destroy -> " + Response);
                            this.AddToastMessage("Error Query", "Terjadi kesalahan di pengambilan data", ToastType.Error);
                        }
                    }
                    else
                    {
                        _Logger.Warn("Menu_Destroy -> Paremeter tidak valid");
                        this.AddToastMessage("Error Parameter", "Paremeter tidak valid", ToastType.Error);
                    }
                }
                else
                {
                    _Logger.Error("Menu_Destroy -> Mohon maaf permintaan kami tolak karena anda belum login");
                    this.AddToastMessage("Error Login", "Mohon maaf permintaan kami tolak karena anda belum login", ToastType.Error);
                }
                return RedirectToAction("Menu", "Settings");
            }
            catch (Exception e)
            {
                _Logger.Fatal("Menu_Destroy -> " + e.Message.ToString());
                this.AddToastMessage("Error", e.Message.ToString(), ToastType.Error);
               return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
            }
        }

        [PelindoValidateAntiForgeryToken]
        public ActionResult Menu_StoreOrderMenu(IEnumerable<OrderMenuModels> Requests)
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    if (ModelState.IsValid)
                    {
                        var claimsIdentity = User.Identity as ClaimsIdentity;
                        string
                            username = claimsIdentity.FindFirst("USERNAME").Value,
                            hakakses = claimsIdentity.FindFirst("HAKAKSES").Value;

                        ViewBag.USERNAME = username;
                        ViewBag.HAKAKSES = hakakses;

                        var Response = _Settings.Menu_StoreOrderMenu(Requests);

                        if (Response == "S")
                        {
                            _Logger.Info("By " + username + " : Menu_StoreOrderMenu -> Data berhasil di simpan");
                            return Json(new { Response_Type = Response, Response_Text = "Struktur berhasil di ubah" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            _Logger.Fatal("By " + username + " : Menu_StoreOrderMenu -> " + Response);
                            return Json(new { Response_Type = "EQ", Response_Text = "Terjadi kesalahan di pengambilan data" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        _Logger.Warn("Menu_StoreOrderMenu -> Paremeter tidak valid");
                        return Json(new { Response_Type = "EM", Response_Text = "Paremeter tidak valid" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    _Logger.Error("Menu_StoreOrderMenu -> Mohon maaf permintaan kami tolak karena anda belum login");
                    return Json(new { Response_Type = "EL", Response_Text = "Mohon maaf permintaan kami tolak karena anda belum login" }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception e)
            {
                _Logger.Fatal("Menu_StoreOrderMenu -> " + e.Message.ToString());
                return Json(new { Response_Type = "EE", Response_Text = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        // GET: HakAkses
        #region Hak Akses
        public ActionResult HakAkses()
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    var claimsidentity = User.Identity as ClaimsIdentity;
                    string
                        username = claimsidentity.FindFirst("USERNAME").Value,
                        hakakses = claimsidentity.FindFirst("HAKAKSES").Value;
                    ViewBag.USERNAME = username;
                    ViewBag.HAKAKSES = hakakses;

                    _Logger.Info("By " + username + " : HakAkses -> Success returning to HakAkses/Index");
                    return View("HakAkses/Index");
                }
                else
                {
                    _Logger.Error("HakAkses -> Mohon maaf permintaan kami tolak karena anda belum login");
                   return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
                }
            }
            catch (Exception e)
            {
                _Logger.Fatal("HakAkses -> " + e.Message.ToString());
               return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
            }
        }

        [PelindoValidateAntiForgeryToken]
        public ActionResult HakAkses_GetHakAkses()
        {
            int length = Convert.ToInt16(Request["length"]);
            int start = Convert.ToInt16(Request["start"]);
            int draw = Convert.ToInt16(Request["draw"]);

            var order = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var orderDir = Convert.ToString(Request["order[0][dir]"]);
            string search = Convert.ToString(Request["search[value]"]);
            int recFilter = 0;

            IEnumerable<HakAksesModels> data = null;

            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    var claimsidentity = User.Identity as ClaimsIdentity;
                    string
                        username = claimsidentity.FindFirst("USERNAME").Value,
                        hakakses = claimsidentity.FindFirst("HAKAKSES").Value;

                    string Where = "";
                    string OrderBy = "";
                    string Limit = "";
                    string Select = "HAK_ID, HAK_KODE, HAK_NAMA";

                    // Sorting.   
                    if (!(string.IsNullOrEmpty(order) && string.IsNullOrEmpty(orderDir)))
                    {
                        if (order == "Hak_Kode")
                        {
                            order = "HAK_KODE";
                        }
                        else if (order == "Hak_Nama")
                        {
                            order = "HAK_NAMA";
                        }
                        OrderBy = order + " " + orderDir.ToUpper();
                    }

                    // Apply pagination.   
                    var total = start + length;
                    var mulai = 0;
                    if (start != 0)
                        mulai = start + 1;

                    Limit = "R BETWEEN " + mulai + " AND " + total;

                    if (!string.IsNullOrEmpty(search) && !string.IsNullOrWhiteSpace(search) && search.Length >= 2)
                    {
                        // Apply search   
                        Where = " HAK_KODE LIKE '%" + search + "%' OR UPPER(HAK_NAMA) LIKE '%" + search.ToUpper() + "%'";

                        // get data 
                        data = _Settings.HakAkses_GetHakAkses(Select, Where, Limit, OrderBy);
                        // Filter record count.   
                        recFilter = _GlobalHelper.GetCountRowTable("COUNT(*)","TPSOL_HAK_AKSES", " WHERE " + Where,"");
                    }
                    else
                    {
                        // get data 
                        data = _Settings.HakAkses_GetHakAkses(Select, Where, Limit, OrderBy);
                        // Filter record count. 
                        recFilter = _GlobalHelper.GetCountRowTable("COUNT(*)", "TPSOL_HAK_AKSES", "","");
                    }

                    _Logger.Info("By " + username + " : HakAkses_GetHakAkses -> Success returning data HakAkses_GetHakAkses");
                    return Json(new { draw = draw, recordsTotal = data.Count(), recordsFiltered = recFilter, data = data }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    _Logger.Error("HakAkses_GetHakAkses -> Mohon maaf permintaan kami tolak karena anda belum login");
                    return Json(new { Response_Type = "E", Response_Text = "Mohon maaf permintaan kami tolak karena anda belum login" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                _Logger.Fatal("HakAkses_GetHakAkses -> " + e.Message.ToString());
                return Json(new { Response_Type = "E", Response_Text = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [PelindoValidateAntiForgeryToken]
        public ActionResult HakAkses_Store(HakAksesModels Requests)
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    if (ModelState.IsValid)
                    {
                        var claimsIdentity = User.Identity as ClaimsIdentity;
                        string
                            username = claimsIdentity.FindFirst("USERNAME").Value,
                            hakakses = claimsIdentity.FindFirst("HAKAKSES").Value;

                        ViewBag.USERNAME = username;
                        ViewBag.HAKAKSES = hakakses;

                        var Response = _Settings.HakAkses_Store(Requests);

                        if (Response == "S")
                        {
                            _Logger.Info("By " + username + " : HakAkses_Store -> Data berhasil di simpan");
                            this.AddToastMessage("Sukses", "Data berhasil di simpan", ToastType.Success);
                        }
                        else
                        {
                            _Logger.Fatal("By " + username + " : HakAkses_Store -> " + Response);
                            this.AddToastMessage("Error Query", "Terjadi kesalahan di pengambilan data", ToastType.Error);
                        }
                    }
                    else
                    {
                        _Logger.Warn("HakAkses_Store -> Paremeter tidak valid");
                        this.AddToastMessage("Error Parameter", "Paremeter tidak valid", ToastType.Error);
                    }
                }
                else
                {
                    _Logger.Error("HakAkses_Store -> Mohon maaf permintaan kami tolak karena anda belum login");
                    this.AddToastMessage("Error Login", "Mohon maaf permintaan kami tolak karena anda belum login", ToastType.Error);
                }
                return RedirectToAction("HakAkses", "Settings");
            }
            catch (Exception e)
            {
                _Logger.Fatal("HakAkses_Store -> " + e.Message.ToString());
                this.AddToastMessage("Error", e.Message.ToString(), ToastType.Error);
               return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
            }
        }

        [PelindoValidateAntiForgeryToken]
        public ActionResult HakAkses_Destroy(string id)
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    if (ModelState.IsValid)
                    {
                        var claimsIdentity = User.Identity as ClaimsIdentity;
                        string
                            username = claimsIdentity.FindFirst("USERNAME").Value,
                            hakakses = claimsIdentity.FindFirst("HAKAKSES").Value;

                        ViewBag.USERNAME = username;
                        ViewBag.HAKAKSES = hakakses;

                        var DecodeBytes = Convert.FromBase64String(id);
                        string Decoded = Encoding.UTF8.GetString(DecodeBytes);

                        var Response = _Settings.HakAkses_Destroy(Decoded);

                        if (Response == "S")
                        {
                            _Logger.Info("By " + username + " : HakAkses_Destroy -> Data berhasil di hapus");
                            this.AddToastMessage("Sukses", "Data berhasil di hapus", ToastType.Success);
                        }
                        else
                        {
                            _Logger.Fatal("By " + username + " : HakAkses_Destroy -> " + Response);
                            this.AddToastMessage("Error Query", "Terjadi kesalahan di pengambilan data", ToastType.Error);
                        }
                    }
                    else
                    {
                        _Logger.Warn("HakAkses_Destroy -> Paremeter tidak valid");
                        this.AddToastMessage("Error Parameter", "Paremeter tidak valid", ToastType.Error);
                    }
                }
                else
                {
                    _Logger.Error("HakAkses_Destroy -> Mohon maaf permintaan kami tolak karena anda belum login");
                    this.AddToastMessage("Error Login", "Mohon maaf permintaan kami tolak karena anda belum login", ToastType.Error);
                }
                return RedirectToAction("HakAkses", "Settings");
            }
            catch (Exception e)
            {
                _Logger.Fatal("HakAkses_Destroy -> " + e.Message.ToString());
                this.AddToastMessage("Error", e.Message.ToString(), ToastType.Error);
               return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
            }
        }
        public ActionResult HakAkses_Edit(string id)
        {
            List<HakAksesModels> HakAkses = null;
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    var claimsidentity = User.Identity as ClaimsIdentity;
                    string
                        username = claimsidentity.FindFirst("USERNAME").Value,
                        hakakses = claimsidentity.FindFirst("HAKAKSES").Value;
                    ViewBag.USERNAME = username;
                    ViewBag.HAKAKSES = hakakses;
                    ViewBag.HAK_ID = id;

                    var DecodeBytes = Convert.FromBase64String(id);
                    string Decoded = Encoding.UTF8.GetString(DecodeBytes);

                    HakAkses = _Settings.HakAkses_Edit(Decoded);

                    if (HakAkses.Count() > 0)
                    {
                        _Logger.Info("By " + username + " : HakAkses_Edit -> Success returning data HakAkses_Edit");
                    }
                    else
                    {
                        _Logger.Fatal("By " + username + " : HakAkses_Edit -> Error Exception, Terjadi Kesalahan Query");
                    }
                    return View("HakAkses/Index", HakAkses);
                }
                else
                {
                    _Logger.Error("HakAkses_Edit -> Mohon maaf permintaan kami tolak karena anda belum login");
                   return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
                }
            }
            catch (Exception e)
            {
                _Logger.Fatal("HakAkses_Edit -> " + e.Message.ToString());
               return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
            }
        }

        [PelindoValidateAntiForgeryToken]
        public ActionResult HakAkses_Update(string id, HakAksesModels Requests)
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    if (ModelState.IsValid)
                    {
                        var claimsIdentity = User.Identity as ClaimsIdentity;
                        string
                            username = claimsIdentity.FindFirst("USERNAME").Value,
                            hakakses = claimsIdentity.FindFirst("HAKAKSES").Value;

                        ViewBag.USERNAME = username;
                        ViewBag.HAKAKSES = hakakses;

                        var DecodeBytes = Convert.FromBase64String(id);
                        string Decoded = Encoding.UTF8.GetString(DecodeBytes);

                        var Response = _Settings.HakAkses_Update(Decoded, Requests);

                        if (Response == "S")
                        {
                            _Logger.Info("By " + username + " : HakAkses_Update -> Data berhasil di ubah");
                            this.AddToastMessage("Sukses", "Data berhasil di ubah", ToastType.Success);
                        }
                        else
                        {
                            _Logger.Fatal("By " + username + " : HakAkses_Update -> " + Response);
                            this.AddToastMessage("Error Query", "Terjadi kesalahan di pengambilan data", ToastType.Error);
                        }
                    }
                    else
                    {
                        _Logger.Warn("HakAkses_Update -> Paremeter tidak valid");
                        this.AddToastMessage("Error Parameter", "Paremeter tidak valid", ToastType.Error);
                    }
                }
                else
                {
                    _Logger.Error("HakAkses_Update -> Mohon maaf permintaan kami tolak karena anda belum login");
                    this.AddToastMessage("Error Login", "Mohon maaf permintaan kami tolak karena anda belum login", ToastType.Error);
                }
                return RedirectToAction("HakAkses", "Settings");
            }
            catch (Exception e)
            {
                _Logger.Fatal("HakAkses_Update -> " + e.Message.ToString());
                this.AddToastMessage("Error", e.Message.ToString(), ToastType.Error);
               return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
            }
        }

        [PelindoValidateAntiForgeryToken]
        public ActionResult HakAkses_StoreAkses(IEnumerable<AksesMenuModels> Request)
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    if (ModelState.IsValid)
                    {
                        var claimsIdentity = User.Identity as ClaimsIdentity;
                        string
                            username = claimsIdentity.FindFirst("USERNAME").Value,
                            hakakses = claimsIdentity.FindFirst("HAKAKSES").Value;

                        ViewBag.USERNAME = username;
                        ViewBag.HAKAKSES = hakakses;

                        var Response = _Settings.HakAkses_StoreAkses(Request);

                        if (Response == "S")
                        {
                            _Logger.Info("By " + username + " : HakAkses_StoreAkses -> Data berhasil di simpan");
                            return Json(new { Response_Type = Response, Response_Text = "Struktur berhasil di ubah" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            _Logger.Fatal("By " + username + " : HakAkses_StoreAkses -> " + Response);
                            return Json(new { Response_Type = "EQ", Response_Text = "Terjadi kesalahan di pengambilan data" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        _Logger.Warn("HakAkses_StoreAkses -> Paremeter tidak valid");
                        return Json(new { Response_Type = "EM", Response_Text = "Paremeter tidak valid" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    _Logger.Error("HakAkses_StoreAkses -> Mohon maaf permintaan kami tolak karena anda belum login");
                    return Json(new { Response_Type = "EL", Response_Text = "Mohon maaf permintaan kami tolak karena anda belum login" }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception e)
            {
                _Logger.Fatal("HakAkses_StoreAkses -> " + e.Message.ToString());
                return Json(new { Response_Type = "EE", Response_Text = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult HakAkses_AksesMenu(string id)
        {
            List<AksesMenuModels> HakAkses = null;
            try
            {
                if (User.Identity.IsAuthenticated)
                {

                    var claimsidentity = User.Identity as ClaimsIdentity;
                    string
                        username = claimsidentity.FindFirst("USERNAME").Value,
                        hakakses = claimsidentity.FindFirst("HAKAKSES").Value;
                    ViewBag.USERNAME = username;
                    ViewBag.HAKAKSES = hakakses;
                    ViewBag.Check = "checked";

                    var DecodeBytes = Convert.FromBase64String(id);
                    string Decoded = Encoding.UTF8.GetString(DecodeBytes);

                    ViewBag.Hakkodekey = id;
                    HakAkses = _Settings.HakAkses_AksesMenu(Decoded);

                    if (HakAkses.Count() > 0)
                    {
                        _Logger.Info("By " + username + " : HakAkses_AksesMenu ->  Success returning data HakAkses_AksesMenu");
                    }
                    else
                    {
                        _Logger.Fatal("By " + username + " : HakAkses_AksesMenu -> Error Exception, Terjadi Kesalahan Query");
                    }
                    return View("HakAkses/Index", HakAkses);
                }
                else
                {
                    _Logger.Error("HakAkses_AksesMenu -> Mohon maaf permintaan kami tolak karena anda belum login");
                   return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
                }
            }
            catch (Exception e)
            {
                _Logger.Fatal("HakAkses_AksesMenu -> " + e.Message.ToString());
               return RedirectToAction("LogIn", "Auth", new { returnUrl = Server.UrlEncode(Request.Url.LocalPath) });
            }
        }
        #endregion
    }
}