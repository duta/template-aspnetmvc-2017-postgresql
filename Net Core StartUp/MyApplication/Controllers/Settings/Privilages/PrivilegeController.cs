﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyApplication.Entities;
using Microsoft.AspNetCore.Authorization;

namespace MyApplication.Controllers
{
    [Authorize]
    [Area("Settings")]
    public class PrivilegeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PrivilegeController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Privilege
        public async Task<IActionResult> Index()
        {
            return View(await _context.RoleMenus.ToListAsync());
        }

        // GET: Privilege/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationRoleMenu = await _context.RoleMenus
                .SingleOrDefaultAsync(m => m.Id == id);
            if (applicationRoleMenu == null)
            {
                return NotFound();
            }

            return View(applicationRoleMenu);
        }

        // GET: Privilege/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Privilege/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Kd_cabang,Kd_Terminal")] ApplicationRoleMenu applicationRoleMenu)
        {
            if (ModelState.IsValid)
            {
                _context.Add(applicationRoleMenu);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(applicationRoleMenu);
        }

        // GET: Privilege/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationRoleMenu = await _context.RoleMenus.SingleOrDefaultAsync(m => m.Id == id);
            if (applicationRoleMenu == null)
            {
                return NotFound();
            }
            return View(applicationRoleMenu);
        }

        // POST: Privilege/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Kd_cabang,Kd_Terminal")] ApplicationRoleMenu applicationRoleMenu)
        {
            if (id != applicationRoleMenu.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(applicationRoleMenu);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ApplicationRoleMenuExists(applicationRoleMenu.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(applicationRoleMenu);
        }

        // GET: Privilege/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationRoleMenu = await _context.RoleMenus
                .SingleOrDefaultAsync(m => m.Id == id);
            if (applicationRoleMenu == null)
            {
                return NotFound();
            }

            return View(applicationRoleMenu);
        }

        // POST: Privilege/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var applicationRoleMenu = await _context.RoleMenus.SingleOrDefaultAsync(m => m.Id == id);
            _context.RoleMenus.Remove(applicationRoleMenu);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ApplicationRoleMenuExists(string id)
        {
            return _context.RoleMenus.Any(e => e.Id == id);
        }
    }
}
