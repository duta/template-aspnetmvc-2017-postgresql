﻿using MyApplication.Entities;
using System.Collections.Generic;

namespace MyApplication.Models
{
    public class MenuViewModel
    {
        public ICollection<MenuItem> MenuItems { get; set; }
        public ApplicationUser User { get; set; }
        public IEnumerable<ApplicationRole> Roles { get; set; }
        public ICollection<MenuPermission> Permissions { get; set; }
    }
}
