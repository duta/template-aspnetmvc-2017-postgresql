﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace P3Apps.Models
{
    public class HakAksesModels
    {
        public int Hak_ID { get; set; }
        public string Hak_Kode { get; set; }
        public string Hak_Nama { get; set; }
        public string Hak_Server { get; set; }
    }
}