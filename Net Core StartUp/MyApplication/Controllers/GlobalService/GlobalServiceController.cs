﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyApplication.Entities;
using System.Security.Claims;
using SpinerService;
using Microsoft.AspNetCore.Authorization;

namespace MyApplication.Controllers.Service
{
    [Authorize]
    public class GlobalServiceController : Controller
    {
        private PelindoServiceClient _Service = new PelindoServiceClient();
        [HttpGet]
        [ValidateAntiForgeryToken]
        public async Task<KapalResponse> GetVessel(string Request)
        {
            if (ModelState.IsValid)
            {
                var _CI = User.Identity as ClaimsIdentity;
                return await _Service.GetKapalAsync(new KapalRequest
                {
                    Filter = Request,
                    Validated = new IsValidated
                    {
                        GUID = _CI.FindFirst("GUID").Value,
                        USERNAME = _CI.FindFirst("USERNAME").Value,
                        TOKEN = _CI.FindFirst("TOKEN").Value
                    }
                });
            }
            else
            {
                return new KapalResponse { Status = "EV", Result = null, Message = "Your Input data is not Valid. #02" };
            }
        }
        [HttpGet]
        [ValidateAntiForgeryToken]
        public async Task<PelabuhanResponse> GetPelabuhan(string Request)
        {
            if (ModelState.IsValid)
            {
                var _CI = User.Identity as ClaimsIdentity;
                return await _Service.GetPelabuhanAsync(new PelabuhanRequest
                {
                    Filter = Request,
                    Validated = new IsValidated
                    {
                        GUID = _CI.FindFirst("GUID").Value,
                        USERNAME = _CI.FindFirst("USERNAME").Value,
                        TOKEN = _CI.FindFirst("TOKEN").Value
                    }
                });
            }
            else
            {
                return new PelabuhanResponse { Status = "EV", Result = null, Message = "Your Input data is not Valid. #02" };
            }
        }
        [HttpGet]
        [ValidateAntiForgeryToken]
        public async Task<DermagaResponse> GetDermaga(string Request)
        {
            if (ModelState.IsValid)
            {
                var _CI = User.Identity as ClaimsIdentity;
                return await _Service.GetDermagaAsync(new DermagaRequest
                {
                    KdCabang = _CI.FindFirst("KD_CABANG").Value,
                    KdTerminal = _CI.FindFirst("KD_TERMINAL").Value,
                    Filter = Request,
                    Validated = new IsValidated
                    {
                        GUID = _CI.FindFirst("GUID").Value,
                        USERNAME = _CI.FindFirst("USERNAME").Value,
                        TOKEN = _CI.FindFirst("TOKEN").Value
                    }
                });
            }
            else
            {
                return new DermagaResponse { Status = "EV", Result = null, Message = "Your Input data is not Valid. #02" };
            }
        }
        [HttpGet]
        [ValidateAntiForgeryToken]
        public async Task<PelangganResponse> GetPelanggan(string Request)
        {
            if (ModelState.IsValid)
            {
                var _CI = User.Identity as ClaimsIdentity;
                return await _Service.GetPelangganAsync(new PelangganRequest
                {
                    KD_CABANG = _CI.FindFirst("KD_CABANG").Value,
                    Filter = Request,
                    Validated = new IsValidated
                    {
                        GUID = _CI.FindFirst("GUID").Value,
                        USERNAME = _CI.FindFirst("USERNAME").Value,
                        TOKEN = _CI.FindFirst("TOKEN").Value
                    }
                });
            }
            else
            {
                return new PelangganResponse { Status = "EV", Result = null, Message = "Your Input data is not Valid. #02" };
            }
        }
    }
}