﻿using System.Configuration;

namespace P3Apps.Models
{
    public class DatabaseHelper
    {
        public static string GetConnectionString(string name = "default")
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }
    }
}