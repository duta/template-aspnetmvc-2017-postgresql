﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(P3Apps.Startup))]
namespace P3Apps
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
