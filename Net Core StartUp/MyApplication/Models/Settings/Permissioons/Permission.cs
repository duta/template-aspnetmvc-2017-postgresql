﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MyApplication.Models
{
    public class Permission
    {
        [Key, Required]
        public string Id { get; set; }
        [Display(Name = "Permission Name")]
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Permission()
        {
            this.CreatedDate = DateTime.Now;
            this.UpdatedDate = DateTime.Now;
        }
    }
}
