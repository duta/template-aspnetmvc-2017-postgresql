﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using MyApplication.Entities;
using Microsoft.AspNetCore.Mvc.Razor;
using MyApplication.Security.Midleware;
using DataTables.AspNet.AspNetCore;
using MyApplication.Filters;

namespace MyApplication
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            MyService.Configuration = configuration;
            _hostingEnv = env;
        }
        public IConfiguration Configuration { get; }
        public static IHostingEnvironment _hostingEnv;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddDbContext<ApplicationDbContext>(options =>
            //    options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"))
            //);


            //services.AddIdentity<ApplicationUser, ApplicationRole>()
            //    .AddEntityFrameworkStores<ApplicationDbContext>()
            //    .AddDefaultTokenProviders();

            services.AddOptions();
            services.Configure<ValidateOriginOptions>(Configuration);

            // If you want to tweak Identity cookies, they're no longer part of IdentityOptions.
            services.AddAuthentication(options =>
            {
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;

            })
            .AddCookie(options =>
            {
                options.LoginPath = new PathString("/Account/Login");
                options.AccessDeniedPath = new PathString("/Account/AccessDenied");
                options.LogoutPath = new PathString("/Account/SignOff");
            });

         //   var skipSSL = Configuration.GetValue<bool>("LocalTest:skipSSL");

            //services.Configure<MvcOptions>(options =>
            //{
            //    if (_hostingEnv.IsDevelopment() && !skipSSL)
            //    {
            //        options.Filters.Add(new RequireHttpsAttribute());
            //    }
            //});

            services.AddSingleton<IRazorViewEngine, CustomViewEngine>();
            
            services.AddResponseCompression(options =>
            {
                options.MimeTypes =  Helpers.DefaultMimeTypes;
            });

            services.AddAntiforgery(o => o.HeaderName = "X-XSRF-TOKEN");

            services.AddMvc(opts =>
            {
                opts.Filters.AddService(typeof(AntiforgeryCookieResultFilter));
                opts.Filters.AddService(typeof(ValidateOriginAuthorizationFilter));
            });

            services.AddTransient<AntiforgeryCookieResultFilter>();
            services.AddTransient<ValidateOriginAuthorizationFilter>();

            services.Configure<RazorViewEngineOptions>(options => {
                options.ViewLocationExpanders.Add(new ViewLocationExpander());
            });

            services.RegisterDataTables();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,  ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            //_hostingEnv.EnvironmentName = EnvironmentName.Production;

            if (_hostingEnv.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseDeveloperExceptionPage();
                app.UseResponseCompression();
                app.UseExceptionHandler("/Dashboard/Error");
            }

            app.Use(async (context, next) =>
            {
                await next.Invoke();
                //After going down the pipeline check if we 404'd. 
                if (context.Response.StatusCode == StatusCodes.Status401Unauthorized)
                {
                    //await context.Response.WriteAsync("Woops! We 404'd");
                    await context.Response.WriteAsync("401");
                    //await context.Response.w(new { status = "401", message = "ERROR 401" });
                }
                else if (context.Response.StatusCode == StatusCodes.Status404NotFound)
                {
                    //await context.Response.WriteAsync("Woops! We 404'd");
                    context.Response.Redirect("/Account/PageNotFound");
                }
                else if (context.Response.StatusCode == StatusCodes.Status500InternalServerError)
                {
                    //await context.Response.WriteAsync("Woops! We 404'd");
                    context.Response.Redirect("/Account/InternalServerError");
                }
            });

            app.UseSecurityHeadersMiddleware(new SecurityHeadersBuilder()
              .AddDefaultSecurePolicy()
              .AddCustomHeader("X-XIERS-APP", "PT. PELABUHAN INDONESIA III (Persero)")
            );

            app.UseSecurityMiddleware();

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(name: "areaRoute",
                    template: "{area:exists}/{controller}/{action}",
                    defaults: new { controller = "Privilege", action = "Index" });

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Dashboard}/{action=Index}/{id?}");
            });
        }
    }
}
