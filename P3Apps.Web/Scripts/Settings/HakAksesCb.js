﻿var MyFunction = function () {

    var listsd = { _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) { var t = ""; var n, r, i, s, o, u, a; var f = 0; e = listsd._utf8_encode(e); while (f < e.length) { n = e.charCodeAt(f++); r = e.charCodeAt(f++); i = e.charCodeAt(f++); s = n >> 2; o = (n & 3) << 4 | r >> 4; u = (r & 15) << 2 | i >> 6; a = i & 63; if (isNaN(r)) { u = a = 64 } else if (isNaN(i)) { a = 64 } t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a) } return t }, decode: function (e) { var t = ""; var n, r, i; var s, o, u, a; var f = 0; e = e.replace(/[^A-Za-z0-9+/=]/g, ""); while (f < e.length) { s = this._keyStr.indexOf(e.charAt(f++)); o = this._keyStr.indexOf(e.charAt(f++)); u = this._keyStr.indexOf(e.charAt(f++)); a = this._keyStr.indexOf(e.charAt(f++)); n = s << 2 | o >> 4; r = (o & 15) << 4 | u >> 2; i = (u & 3) << 6 | a; t = t + String.fromCharCode(n); if (u != 64) { t = t + String.fromCharCode(r) } if (a != 64) { t = t + String.fromCharCode(i) } } t = listsd._utf8_decode(t); return t }, _utf8_encode: function (e) { e = e.replace(/rn/g, "n"); var t = ""; for (var n = 0; n < e.length; n++) { var r = e.charCodeAt(n); if (r < 128) { t += String.fromCharCode(r) } else if (r > 127 && r < 2048) { t += String.fromCharCode(r >> 6 | 192); t += String.fromCharCode(r & 63 | 128) } else { t += String.fromCharCode(r >> 12 | 224); t += String.fromCharCode(r >> 6 & 63 | 128); t += String.fromCharCode(r & 63 | 128) } } return t }, _utf8_decode: function (e) { var t = ""; var n = 0; var r = c1 = c2 = 0; while (n < e.length) { r = e.charCodeAt(n); if (r < 128) { t += String.fromCharCode(r); n++ } else if (r > 191 && r < 224) { c2 = e.charCodeAt(n + 1); t += String.fromCharCode((r & 31) << 6 | c2 & 63); n += 2 } else { c2 = e.charCodeAt(n + 1); c3 = e.charCodeAt(n + 2); t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63); n += 3 } } return t } };

    var publicFunction = function () {

        $('.slimscroll').slimScroll({
            height: '450px',
            //railVisible: true,
            alwaysVisible: true
        });

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "1000",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        $("#checkall").click(function () {
            if ((this).checked == true) {
                $('.ca-target .md-check').prop('checked', true);
            } else {
                $('.ca-target .md-check').prop('checked', false);
            }
        });

        $(".ca-target .md-check").click(function () {
            if ((this).checked == true) {
                childcheck($(this).val(), true);
                headercheck($(this).data("id"), true);
            } else {
                childcheck($(this).val(), false);
                headercheck($(this).data("id"), false);
            }
        });

        var childcheck = function (data, check) {
            $(".cbp-" + data).prop('checked', check);
        }

        var headercheck = function (data, check) {
            $(".cb-" + data).prop('checked', check);
        }

        $('#save-menu').click(function () {

            App.blockUI({
                boxed: true,
                message: 'Menyimpan data...'
            });

            var pusatMenu = [];
            $(".ca-target .md-check:checked").each(function () {
                pusatMenu.push(listsd.encode(this.value));
                //pusatMenu.push(this.value);
            });
            var obj = [];

            if (pusatMenu.length != 0) {
                for (i = 0 ; i < pusatMenu.length; i++) {
                    obj.push({ "HAK_KODE_KEY": $("#get-hakkodekey").val(), "MENU_ID_KEY": pusatMenu[i] });
                }
            }
            else {
                obj.push({ "HAK_KODE_KEY": $("#get-hakkodekey").val() });
            }

            //console.log(obj);

            $.ajax({
                type: "POST",
                url: "/Settings/HakAkses_StoreAkses",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(obj),
                headers: {
                    'CSRF-TOKEN_PELINDO3': $("[name=csrf-token]").attr("content")
                },
                success: function (data) {
                    if (data.Response_Type == "S") {
                        App.unblockUI();
                        toastr.success(data.Response_Text, "Sukses");
                        setInterval(function () {
                            location.href = "/Settings/HakAkses_AksesMenu/" + $("#get-hakkodekey").val();
                        }, 1000);
                    } else {
                        App.unblockUI();
                        toastr.error(data.Response_Text, "Error Kode : " + data.Response_Type);
                    }
                },
                error: function () {
                    App.unblockUI();
                    toastr.error("Data menu gagal disimpan", "Error");
                }
            });

        });

    }

    var HandleTable = function () {
        var table = $('#datatable')
        table.DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "language": { "url": "/assets/global/plugins/datatables/plugins/i18n/Indonesian.json" },

            "ajax": {
                "url": "/Settings/HakAkses_GetHakAkses",
                "type": "POST",
                "datatype": "json",
                "headers": {
                    'CSRF-TOKEN_PELINDO3': $("[name=csrf-token]").attr("content")
                }
            },
            "columns": [
                { "data": "Hak_Kode", "name": "Hak_Kode" },
                { "data": "Hak_Nama", "name": "Hak_Nama" },
                {
                    "data": "Hak_Kode", "name": "Hak_Kode",
                    "render": function (data, type, full, meta) {
                        var html;
                        html = '<div class="text-center">';
                        html += '<div class="btn-group btn-group-solid">';
                        html += '<a href="/Settings/HakAkses_AksesMenu/' + listsd.encode(data) + '" class="btn green btn-xs" title="Lihat Role"><i class="fa fa-list-ul"></i></a>';
                        html += '<a href="/Settings/HakAkses_Edit/' + listsd.encode(full.Hak_ID.toString()) + '" class="btn purple btn-xs" title="Edit Hak Akses"><i class="fa fa-edit"></i></a>';
                        html += '<a href="#hapus" onclick="ConfirmDelete(\'' + '/Settings/HakAkses_Destroy/' + listsd.encode(data) + '\')" class="btn red btn-xs" title="Hapus Hak Akses"><i class="fa fa-trash"></i></a>';
                        html += '</div>';
                        html += '</div>';
                        return html;
                    }
                }
            ],
            "columnDefs": [{
                'orderable': false,
                'targets': [2]
            }, {
                "searchable": false,
                "targets": [2]
            }],
            "order": [
                [0, "asc"]
            ],
            "lengthMenu": [
                [5, 10, 25, 50, 100],
                [5, 10, 25, 50, 100] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,
            "pagingType": 'bootstrap_extended',
        });
    }

    var handleValidation1 = function () {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation
        var form1 = $('#form_validation_1');
        var error1 = $('.alert-danger', form1);
        //var success1 = $('.alert-success', form1);

        $(".button-clear").click(function () {
            $('#form_validation_1 :input').val('');
            $('.form-group').removeClass('has-success');
            $('.form-group').removeClass('has-error');
            $('.help-block-error').remove();
            error1.hide();
        });

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            messages: {
                Hak_Kode: {
                    maxlength: jQuery.validator.format("Maksimal 75 Karakter"),
                    required: "Data Harus Diisi"
                },
                Hak_Nama: {
                    maxlength: jQuery.validator.format("Maksimal 100 Karakter"),
                    required: "Data Harus Diisi"
                }
            },
            rules: {
                Hak_Kode: {
                    required: true,
                    maxlength: 75
                },
                Hak_Nama: {
                    required: true,
                    maxlength: 100
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                error1.show();
                App.scrollTo(error1, -200);
            },

            errorPlacement: function (error, element) {
                    error.insertAfter(element); // for other inputs, just perform default behavior
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
                label    
                    .closest('.form-group').addClass('has-success');
            },

            submitHandler: function (form) {
                error1.hide();
                form.submit();
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            HandleTable();
            handleValidation1();
            publicFunction();
        }
    };

}();


function ConfirmDelete(action) {
    $("#form_delete").attr('action', action);
    $('#DeleteModals').modal('show');
}


$(document).ready(function () {

    MyFunction.init();

});