﻿using MyApplication.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyApplication.Models
{
    public class MenuItem
    {
        [Key, Column(Order = 1)]
        public string Id { get; set; }
        [Display(Name = "Menu Name")]
        public string Name { get; set; }
        public string Url { get; set; }
        public int ParentId { get; set; }
        public string Icon { get; set; }
        public string Order { get; set; }
        public string Category { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public virtual ICollection<MenuItem> Children { get; set; }
        public virtual MenuItem ParentItem { get; set; }
        public virtual ICollection<ApplicationRoleMenu> Roles { get; set; }
        public MenuItem()
        {
            this.CreatedDate = DateTime.Now;
            this.UpdatedDate = DateTime.Now;
            Children = new HashSet<MenuItem>();
            Roles = new HashSet<ApplicationRoleMenu>();
        }
    }
}
