var UINestable = function () {
    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            var dataJson = window.JSON.stringify(list.nestable('serialize'));
            console.log(dataJson);
            console.log(list.nestable('serialize'));
        } else {
            console.log('JSON browser support required for this demo.');
        }
    };
    return {
        //main function to initiate the module
        init: function () {
            // activate Nestable for list 1
            $('#nestable_list_1').nestable({
                group: 1
            })
                .on('change', updateOutput);

            // output initial serialised data
            updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));

            $('#nestable_list_menu').on('click', function (e) {
                var target = $(e.target),
                    action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });
        }
    };
}();
jQuery(document).ready(function () {
    UINestable.init();
});