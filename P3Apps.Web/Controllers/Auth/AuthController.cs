﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using P3Apps.Dapper;
using P3Apps.Models;
//using P3Apps.ServiceP3UserManagement;
using log4net;
using Microsoft.Owin.Security;

namespace P3Apps.Controllers
{
    public class AuthController : Controller
    {
        private static readonly ILog _Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        public ActionResult LogIn(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [PelindoValidateAntiForgeryToken]
        public ActionResult LogIn(LogInModel model)
        {
            if (ModelState.IsValid)
            {
                bool username = string.IsNullOrEmpty(model.Username);
                bool password = string.IsNullOrEmpty(model.Password);

                if (username == false && password == false)
                {
                    string desc = string.Empty;
                    //wsOuthSoap xService;
                    //UserAkunInfo akun;

                    try
                    {
                        //xService = new wsOuthSoapClient();
                        //akun = new UserAkunInfo();
                        //akun = xService.valLoginAkun("66", model.Username, model.Password);

                        if ("S" == "S")
                        //if (akun.responType.Substring(0, 1) == "S")
                        {
                            //var identity = new ClaimsIdentity(new[] {
                            //new Claim(ClaimTypes.Name, akun.NAMA),
                            //new Claim("USERNAME", akun.USERNAME),
                            //new Claim("HAKAKSES", akun.HAKAKSES),
                            //new Claim("HAKAKSES_DESC", akun.HAKAKSES_DESC),
                            //new Claim("KD_CABANG", Int32.Parse(akun.KD_CABANG).ToString("D")),
                            //new Claim("KD_TERMINAL", Int32.Parse(akun.KD_TERMINAL).ToString("D"))

                            //   dummy
                            var identity = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.Name, "RIZQI MUBAROK"),
                            new Claim("USERNAME", "800205114"),
                            new Claim("HAKAKSES", "514"),
                            new Claim("KD_CABANG", "2"),
                            new Claim("KD_TERMINAL", "1")
                            //   end dummy
                            }, "ApplicationCookie");



                            var ctx = Request.GetOwinContext();
                            var authManager = ctx.Authentication;

                            authManager.SignIn(new AuthenticationProperties { IsPersistent = true }, identity);

                            //_Logger.Info("By " + akun.NAMA + " : Dashboard -> Success returning to Home/Index");
                            _Logger.Info("By RIZQI MUBAROK : Dashboard -> Success returning to Home/Index");

                            string decodedUrl = "";
                            if (!string.IsNullOrEmpty(model.ReturnUrl))
                                decodedUrl = Server.UrlDecode(model.ReturnUrl);

                            //Login logic...

                            if (Url.IsLocalUrl(decodedUrl))
                            {
                                return Redirect(decodedUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Home");
                            }
                        }
                        else
                        {
                            //if (akun.responType == "E-003" || akun.responType == "E-018")
                            //{
                            //    ViewBag.Message = akun.URL_CP;
                            //    _Logger.Error("Login -> Response " + akun.responText);
                            //    return View();
                            //}
                            //else
                            //{
                            //    _Logger.Error("Login -> Response " + akun.responText);
                            //    ViewBag.Message = akun.responText;
                            //    return View();
                            //}
                        }

                    }
                    catch (Exception ex)
                    {
                        _Logger.Fatal("Login -> " + ex.Message.ToString());
                        desc = ex.Message;
                        return View();
                    }
                }
                else
                {
                    ViewBag.Message = "Periksa kembali username atau password yang Anda masukkan!";
                    _Logger.Warn("Login -> Periksa kembali username atau password yang Anda masukkan!");
                    return View();
                }
            }
            return View(model);
        }

        public ActionResult LogOut()
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var ctx = Request.GetOwinContext();
                    var authManager = ctx.Authentication;

                    authManager.SignOut("ApplicationCookie");

                    _Logger.Error("By "+ claimsIdentity.FindFirst("USERNAME").Value + " Logout -> Sukses Logout, returning to Auth/Login" );
                    return RedirectToAction("LogIn", "auth");
                }
                else
                {
                    _Logger.Error("Logout -> Sukses Logout");
                    return RedirectToAction("LogIn", "auth");
                }
            }
            catch (Exception e)
            {
                _Logger.Error("Login -> Response " + e.Message.ToString());
                return RedirectToAction("LogIn", "auth");
            }
        }
    }
}