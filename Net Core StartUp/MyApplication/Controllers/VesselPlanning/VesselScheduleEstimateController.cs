﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using DataTables.AspNet.Core;
using DataTables.AspNet.AspNetCore;
using SpinerService;
using System.Security.Claims;

namespace MyApplication.Controllers.VesselPlanning
{

    [Authorize]
    [Area("VesselPlanning")]
    public class VesselScheduleEstimateController : Controller
    {
        private PelindoServiceClient _Service = new PelindoServiceClient();

        // GET: VesselScheduleEstimate
        public ActionResult Index()
        {
            return View();
        }

        // GET: VesselScheduleEstimate/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: VesselScheduleEstimate/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VesselScheduleEstimate/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<StringResponse> Create([FromBody]SaveVesScheduleEstRequest collection)
        {
            StringResponse Response = new StringResponse();

            if (ModelState.IsValid)
            {
                var _CI = User.Identity as ClaimsIdentity;
                Response = await _Service.SaveVesScheduleEstAsync(new SaveVesScheduleEstRequest
                {
                    XKdCabang = _CI.FindFirst("KD_CABANG").Value,
                    XKdTerminal = _CI.FindFirst("KD_TERMINAL").Value,
                    XVesCode = collection.XVesCode,
                    XVesName = collection.XVesName,
                    XAgent = collection.XAgent,
                    XPBM = collection.XPBM,
                    XLOA = collection.XLOA,
                    XKeterangan = collection.XKeterangan,
                    XStatus = "1",
                    XEstArrivalTime = collection.XEstArrivalTime,
                    XEstDepartureTime = collection.XEstDepartureTime,
                    ZKdFrom = collection.ZKdFrom,
                    ZKdTo = collection.ZKdTo,
                    XLastPort = collection.XLastPort,
                    XNextPort = collection.XNextPort,
                    XOriginPort = collection.XOriginPort,
                    XDestPort = collection.XDestPort,
                    XCreatedPort = _CI.FindFirst("USERNAME").Value,
                    XBerthNo = collection.XBerthNo,
                    XGt = collection.XGt,
                    XBendera = collection.XBendera,
                    XPalka = collection.XPalka,
                    XPalkaType = collection.XPalkaType,
                    XLastPortName = collection.XLastPortName,
                    XNextPortName = collection.XNextPortName,
                    XOriginPortName = collection.XOriginPortName,
                    XDestPortName = collection.XDestPortName,
                    XBerthName = collection.XBerthName,
                    XProgramName = _CI.FindFirst("APP_NAME").Value,
                    Validated = new IsValidated
                    {
                        GUID = _CI.FindFirst("GUID").Value,
                        USERNAME = _CI.FindFirst("USERNAME").Value,
                        TOKEN = _CI.FindFirst("TOKEN").Value
                    }
                });

                return Response;
            }
            else
            {
                return new StringResponse { Status = "EV", Result = null, Message = "Your Input data is not Valid. #02" };
            }
        }

        [HttpGet]
        public IActionResult DataVessel()
        {
            return PartialView("_DataVessel");
        }

        // GET: VesselScheduleEstimate/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: VesselScheduleEstimate/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: VesselScheduleEstimate/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: VesselScheduleEstimate/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GetDataVessel(IDataTablesRequest request)
        {
            // Nothing important here. Just creates some mock data.
            var _CI = User.Identity as ClaimsIdentity;
            var data = await _Service.GetVesScheduleEstAsync(new GetVesScheduleEstRequest
            {
                KD_CABANG = _CI.FindFirst("KD_CABANG").Value,
                KD_TERMINAL = _CI.FindFirst("KD_TERMINAL").Value,
                Validated = new IsValidated
                {
                    GUID = _CI.FindFirst("GUID").Value,
                    USERNAME = _CI.FindFirst("USERNAME").Value,
                    TOKEN = _CI.FindFirst("TOKEN").Value
                }
            });

            if (data.Status == "S")
            {
                // Global filtering.
                var filteredData = string.IsNullOrEmpty(request.Search.Value) && string.IsNullOrWhiteSpace(request.Search.Value) ? data.Result : data.Result.Where(p =>
                    p.VES_CODE.ToString().ToLower().Contains(request.Search.Value.ToLower()) ||
                    p.VES_NAME.ToString().ToLower().Contains(request.Search.Value.ToLower()) ||
                    p.MPLG_NAMA.ToString().ToLower().Contains(request.Search.Value.ToLower()) ||
                    p.EST_ARRIVAL_TIME.ToString().ToLower().Contains(request.Search.Value.ToLower()) ||
                    p.EST_DEPARTURE_TIME.ToString().ToLower().Contains(request.Search.Value.ToLower()) ||
                    p.LOKASI.ToString().ToLower().Contains(request.Search.Value.ToLower())
                ).ToArray();


                // Paging filtered data.
                var dataPage = filteredData.Skip(request.Start).Take(request.Length);

                var response = DataTablesResponse.Create(request, data.Result.Count(), filteredData.Count(), dataPage);

                return new DataTablesJsonResult(response, true);
            }
            else
            {
                return Json(data);
            }
        }
    }
}