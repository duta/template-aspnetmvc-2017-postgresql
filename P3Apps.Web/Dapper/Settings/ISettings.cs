﻿using System.Collections.Generic;
using P3Apps.Models;


namespace P3Apps.Dapper
{
    public interface ISettings
    {
        // MENU
        #region Menu
        List<MenuModels> Menu(string id);
        string Menu_Store(MenuModels Request);
        List<MenuModels> Menu_Edit(string id);
        string Menu_Update(string id, MenuModels Request);
        string Menu_Destroy(string id);
        string Menu_StoreOrderMenu(IEnumerable<OrderMenuModels> Request);
        #endregion

        // Hak Akses
        #region Hak Akses
        List<HakAksesModels> HakAkses();
        IEnumerable<HakAksesModels> HakAkses_GetHakAkses(string Select, string Where, string Limit, string OrderBy);
        string HakAkses_Store(HakAksesModels Request);
        string HakAkses_Destroy(string id);
        List<HakAksesModels> HakAkses_Edit(string id);
        string HakAkses_Update(string id, HakAksesModels Request);
        string HakAkses_StoreAkses(IEnumerable<AksesMenuModels> Request);
        List<AksesMenuModels> HakAkses_AksesMenu(string id);
        #endregion
    }
}